module kindtype
  implicit none
  !
  ! This module is used to select the working precision for the whole
  !
  integer, parameter, private :: SINGLE=kind(1.0)    ! Single precision reals
  integer, parameter, private :: DOUBLE=kind(1d0)    ! Double precision reals
  !
  ! Select the precision for the whole code (comment the unwanted precision)
  ! integer, parameter :: rp = SINGLE         ! Single precision
  integer, parameter :: rp = DOUBLE         ! Double precision
  !
end module kindtype
