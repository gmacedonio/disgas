program grd2rough
  ! This program transform a grd file containing land/water information
  ! into file roughness.grd used by disgas
  !
  use kindtype
  use rwgrd
  implicit none
  integer :: nx,ny
  integer, parameter :: ninp=2,nout=3
  real(8) :: xmin,xmax,ymin,ymax,zmin,zmax
  real(rp), pointer :: landuse(:,:)
  real(rp), allocatable :: roughness(:,:)
  real(rp) :: land_roughness=0.1, sea_roughness=0.0
  integer :: narg   ! Number of arguments in the command line
  character(len=100) :: finp, fout
  character(len=80) :: string
  integer :: status
  integer :: grd_type
  integer :: i,j
  !
  !
  narg = command_argument_count()
  !
  if(narg == 4) then
     call get_command_argument(1,finp)
     call get_command_argument(2,fout)
     call get_command_argument(3,string)
     read(string, *) sea_roughness
     call get_command_argument(4,string)
     read(string, *) land_roughness
  else
     write(*,'(''Usage: grd2rough landuse.grd roughness.grd sea_roughness land_roughness'')')
     stop
  endif
  !
  ! Read land use matrix
  open(ninp,file=finp,status='old',err=900)
  call readgrd (ninp,nx,ny,xmin,xmax,ymin,ymax,zmin,zmax,landuse,status)
  if(status /= 0) then
     write(*,*) 'Error reading grid: '//trim(finp)
     stop
  end if
  close(ninp)
  !
  ! Generate roughness matrix
  allocate(roughness(nx,ny))
  do j = 1, ny
     do i = 1, nx
        if(landuse(i,j) == 0.0) then
           roughness(i,j) = sea_roughness
        else
           roughness(i,j) = land_roughness
        end if
     end do
  end do

  ! Write roughness matrix
  !
  ! write(*,*) xmin, xmax
  ! write(*,*) ymin, ymax
  ! write(*,*) (xmax-xmin)/(nx-1)
  ! write(*,*) (ymax-ymin)/(ny-1)
  grd_type = 0    ! 0=ASCII, 1=BINARY
  call wrigrd(fout,grd_type,nx,ny,xmin,xmax,ymin,ymax,roughness,status)

  goto 1000
  !
  ! Errors
900 write(*,*) '***Error: cannot open file '//trim(finp)
  stop
  !
  ! Regular end
1000 continue

end program grd2rough
