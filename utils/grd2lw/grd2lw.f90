program grd2lw
  ! This program transform a grd file containing land/water information
  ! into file topog.lw needed by diagno
  !
  use kindtype
  implicit none
  integer :: nx,ny
  integer, parameter :: ninp=2,nout=3
  real(8) :: xmin,xmax,ymin,ymax,zmin,zmax
  real(rp), pointer :: matrix(:,:) => null()
  integer :: narg   ! Number of arguments in the command line
  character(len=100) :: finp,fout
  integer :: status
  integer :: i,j
  !
  !
  narg = command_argument_count()
  !
  if(narg == 2) then
     call get_command_argument(1,finp)
     call get_command_argument(2,fout)
  else
     write(*,'(''Usage: grd2lw file.grd file.lw'')')
     stop
  endif
  !
  ! Read GRD matrix
  open(ninp,file=finp,status='old',err=900)

  call readgrd (ninp,nx,ny,xmin,xmax,ymin,ymax,zmin,zmax,matrix,status)

  close(ninp)
  !
  ! Write LW matrix
  !
  open(nout,file=fout,status='unknown')
  !
  do j=1,ny
     write(nout,16) (min(1,max(nint(matrix(i,j)),0)),i=1,nx)
  enddo
  !
16 format(5000i2.2)

  close(nout)

  goto 1000
  !
  ! Errors
900 write(*,*) '***Error: cannot open file '//trim(finp)
  stop
  !
  ! Regular end
1000 continue

contains

  subroutine readgrd(ninp,nx,ny,xmin,xmax,ymin,ymax,zmin,zmax,matrix,status)
    !
    ! Read a GRD file and store in a real matrix
    ! The memory for the matrix is allocated by this routine
    !
    use kindtype
    implicit none
    integer, intent(in)  :: ninp    ! Input file unit
    integer, intent(out) :: nx,ny
    integer, intent(out) :: status
    real(rp), pointer :: matrix(:,:)
    real(kind=8), intent(out) :: xmin,xmax,ymin,ymax,zmin,zmax ! Note: real*8
    !
    ! Local variables
    character(len=4) :: magic   ! The magic number
    integer :: i,j,istat
    !
    read(ninp,'(a4)',advance='no') magic
    !
    if(magic=='DSAA') then
       !
       ! ASCII VERSION
       !
       read(ninp,*) nx,ny
       read(ninp,*) xmin,xmax,ymin,ymax,zmin,zmax
       !
       ! Now allocate memory
       allocate(matrix(nx,ny),STAT=istat)

       if(istat /= 0) then
          status = 1    ! Cannot allocate memory
          return
       endif
       read(ninp,*) ((matrix(i,j),i=1,nx),j=1,ny) ! Ascii file is easy to read
       !
    else
       !
       ! Not a GRD file or not supported
       !
       status = 2
       return
    endif

    status = 0  ! Normal termination
    return
  end subroutine readgrd

end program grd2lw
