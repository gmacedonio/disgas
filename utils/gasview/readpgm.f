      subroutine readpgm(ninp,a,maxbuf,nx,ny,ncol)
c.... Read a PGM image (ASCII version)
      dimension a(maxbuf)
      character*2 p2
      character*80 string
      character*80 finp
c
      read(ninp,10) p2
 10   format(a2)
      if(p2.ne.'P2') then
         write(*,*) '***Error in readpgm: Not a PGM file.'
         stop
      endif
      call rdpgmstr(ninp,string)
      read(string,*) nx,ny
      if(nx*ny.gt.maxbuf) then
         write(*,*) '***Error in readpgm:'
         write(*,*) 'Not enough memory to read PGM image.'
         stop
      endif
      call rdpgmstr(ninp,string)
      read(string,*) ncol
      read(ninp,*,end=100) (a(i),i=1,nx*ny)
      goto 1000
c
 100  write(*,*) '***Error in readpgm: Premature end of PPM file.'
      stop
c
 1000 continue
      end
c
      subroutine rdpgmstr(ninp,str)
c.... Read a record in a PGM file (ASCII version)
      character*80 str
 20   continue
      read(ninp,11,end=100,err=100) str
 11   format(a80)
      if(str(1:1).eq.'#') goto 20
      goto 1000
c
 100  write(*,*) '***Error in rdpgmstr: Premature end of PPM file.'
      stop
c
 1000 continue
      end
