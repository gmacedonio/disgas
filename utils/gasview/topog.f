      program topog
      parameter (MAXNX=50, MAXNY=50, MAXNZ=50)
      dimension topg(MAXNX,MAXNY)
      dimension znew(MAXNY,MAXNX,MAXNZ),zzet(MAXNY,MAXNX,MAXNZ)
      dimension zcsi(MAXNY,MAXNX,MAXNZ),zeta(MAXNY,MAXNX,MAXNZ)
      dimension zlayer(MAXNZ),rjac(MAXNY,MAXNX,MAXNZ)
      character*15 fcel,fmat
c
      fcel='diagno.inp'
      fmat='solfatara.mat'
      nuncel = 3
      nunmat = 4
c
c.... Legge numero e dimensioni celle
      open(nuncel,file=fcel,err=801,status='old')
      read(nuncel,*)            ! Skip titolo
      read(nuncel,*) nx
      read(nuncel,*) ny
      read(nuncel,*) nzl
      nz = nzl+1
c.... Check
      if(nx.gt.MAXNX.or.ny.gt.MAXNY.or.nz.gt.MAXNZ) then
         write(*,*) '***Errore: Troppi nodi'
         stop
      endif
      read(nuncel,*) dx
      read(nuncel,*) dy
      read(nuncel,*) (zlayer(i),i=1,nz)
      read(nuncel,*) utmxor     ! Coordinate UTM origine (in KM)
      read(nuncel,*) utmyor
      close(nuncel)
c
c.... Legge Topografia
      open(nunmat,file=fmat,err=802,status='old')
      do j=1,ny
         read(nunmat,*) (topg(i,j),i=1,nx)
      enddo
      close(nunmat)
c
      ztop=250.
      do k=1,nz-1
         dz=(zlayer(k+1)-zlayer(k))
c     zlayer(k)=zlayer(k)-0.5*dz
         do j=1,ny-1
            do i=1,nx-1
c     znew(i,j,k)=ztop*(z(i,j,k)-topg(i,j))/(ztop-topg(i,j))
               znew(i,j,k)=ztop*zlayer(k)/(ztop-topg(i,j))
c     zcsi(i,j,k)=(znew(i+1,j,k)-znew(i,j,k))/dx
c     zeta(i,j,k)=(znew(i,j+1,k)-znew(i,j,k))/dy
c     zzet(i,j,k)=(znew(i,j,k+1)-znew(i,j,k))/dz
               zzet(i,j,k)=ztop/(ztop-topg(i,j))*
     &              (zlayer(k+1)-zlayer(k))/dz

               rjac(i,j,k)=zzet(i,j,k)
c               write(*,*) k,zlayer(k),topg(i,j),znew(i,j,k),zzet(i,j,k)
c     rjac(i,j,k)
            enddo
         enddo
      enddo
c
      if(k.eq.nz) then
         do j=1,ny-1
            do i=1,nx-1
               zzet(i,j,nz)=zzet(i,j,nz-1)
               rjac(i,j,k)=zzet(i,j,k)
c     write(*,*) rjac(i,j,k)
            enddo
         enddo
      elseif(j.eq.ny) then
         do k=1,nz-1
            do i=1,nx-1
               zeta(i,ny,k)=zeta(i,ny-1,k)
               rjac(i,j,k)=zzet(i,j,k)
c     write(*,*) rjac(i,j,k)
            enddo
         enddo
      elseif(i.eq.nx) then
         do k=1,nz-1
            do j=1,ny-1
               zcsi(nx,j,k)=zcsi(nx-1,j,k)
               rjac(i,j,k)=zzet(i,j,k)
               write(*,*) rjac(i,j,k)
            enddo
         enddo
      endif
      goto 1000
 801  write(*,*) '***Errore: non trovo il file '//fcel
      stop
 802  write(*,*) '***Errore: non trovo il file '//fmat
      stop
 1000 continue
      end
