      program kappa2
c     this routine calculate the vertical turbulence atmospheric
c     coefficients by similarity theory based on Bulk Richardson Number.
c     The horizontal are assumed (1) constant or (2) following Pielke(84):
c     hork=alfa*DX*DY*[3/2*(dux/dx)^2+3/2*(duy/dy)^2+2*dux/dx*duy/dy]^1/2
c     (calculati direttamente in femfluid)
c
c.... Author: Antonio Costa and Giovanni Macedonio
c.... Date of first version:  5-Jul-2002
c.... Date of this  version: 25-Jun-2003
c
      parameter (MAXNX=50, MAXNY=50, MAXNZ=20)
      dimension vx(MAXNX,MAXNY,MAXNZ),vy(MAXNX,MAXNY,MAXNZ)
      dimension vertk(MAXNX*MAXNY*MAXNZ),hork(MAXNX*MAXNY*MAXNZ)
      dimension z0(MAXNX,MAXNY),iz0(MAXNX)
      dimension zlayer(MAXNZ),zm(MAXNX,MAXNY)
      dimension n(8)
c
      ninp=7
      nuncel=8
      nout=9
      nwind=10
      nlw=11
c
c.... Legge numero e dimensioni celle
      open(nuncel,file='diagno.inp',status='old')
      read(nuncel,*)            ! Skip titolo
      read(nuncel,*) nx
      read(nuncel,*) ny
      read(nuncel,*) nzl
      nz = nzl+1
      read(nuncel,*) dx
      read(nuncel,*) dy
      read(nuncel,*) (zlayer(i),i=1,nz)
      close(nuncel)
c
      dx=1000.d0*dx             !convert in metres
      dy=1000.d0*dy             !convert in metres
c
      do i=1,nz-1
         dz=(zlayer(i+1)-zlayer(i))
         zlayer(i)=zlayer(i)+0.5*dz
      enddo
c
      open(nwind,file='diagno.out',status='old',
     $     form='unformatted')
      read(nwind) time
      read(nwind) nnx,nny,nnz
      read(nwind) dxk,dyk
      do kk=1,nzl
         do j=1,ny
            read(nwind) (vx(i,j,kk),i=1,nx)
         enddo
      enddo
      do kk=1,nzl
         do j=1,ny
            read(nwind) (vy(i,j,kk),i=1,nx)
         enddo
      enddo
      close(nwind)
c
c
      open(ninp,file='kappa2.inp',status='old')
      read(ninp,*) rl           !Monin-Obukhov lenght
      read(ninp,*) ustar        !roughness vel.
      read(ninp,*) temp         !surface temperature (K)
      read(ninp,*) model        !K_h model
      close(ninp)
c
      omega = 7.292E-5          !Earth angular velocity (1/sec)
      rphi=0.7                  !Latitude (in rads)
      f =2.*omega*sin(rphi)     !Coriolis parameter
c
c     h is the PBL height
c
      nnode=0
      do i=1,nx
         do j=1,ny
            if(rl.ge.0.) then   ! Atmosfera stabile o neutra
               h=rl/3.8*(sqrt(1.+2.28*ustar/(rl*f))-1.)
c
            else                ! Atmosfera instabile
               h=0.3*ustar/f
            endif
            g=9.8
            gh=11.6
            betah=7.8
            prt=0.95
            vkarm=0.4
            do k=1,nz
               nnode=nnode+1
               wstar=(g*zlayer(nz)*h/temp)**0.333333333333 !convective velocity
               zeta=abs(zlayer(k)/h)
               eta = zlayer(k)/rl
c
c     dal jacobson....
c
               if(eta.lt.0.) then
                  vertk(nnode)=vkarm*ustar*zlayer(k)*
     &                 sqrt(1-gh*eta)/prt
               elseif(eta.gt.0.) then
                  vertk(nnode)=vkarm*ustar*zlayer(k)/(prt+betah*eta)
               elseif(eta.eq.0.) then
                  vertk(nnode)=vkarm*ustar*zlayer(k)/prt
               elseif(zeta.gt.1.) then
                  vertk(nnode)=wstar*h*0.0013
               endif
c     write(*,*) zeta,wstar,h,vertk(nnode)
c
c.... scrive hork(i,j,k)
c
               alfa = 5.d0      !empirical parameter
               beta = 1.d0      !empirical parameter
               const= 500.
               if(i.lt.nx.and.j.lt.ny) then
                  dvxdx=(vx(i+1,j,k)-vx(i,j,k))/dx
                  dvydy=(vy(i,j+1,k)-vy(i,j,k))/dy
                  dvxdy=(vx(i,j+1,k)-vx(i,j,k))/dy
                  dvydx=(vy(i,j+1,k)-vy(i,j,k))/dx
               else
                  dvxdx=(vx(nx,j,k)-vx(nx-1,j,k))/dx
                  dvydy=(vy(i,ny,k)-vy(i,ny-1,k))/dy
                  dvxdy=(vx(i,ny,k)-vx(i,ny-1,k))/dy
                  dvydx=(vy(i,ny,k)-vy(i,ny-1,k))/dx
               endif
               if(model.eq.0) then
                  hork = const
               elseif(model.eq.1) then
                  hork(nnode)=alfa*dx*dy*sqrt(1.5*dvxdx**2.+
     &                 1.5*dvydy**2.+
     &                 2.*dvxdx*dvydy) !Pielke(1984)
               else
                  hork(nnode)=beta*dx*dy*sqrt(dvxdx**2.+
     &                 dvydy**2.+2.*dvxdx*dvydy+0.5*dvydx**2.+
     &                 0.5*dvxdy**2.+dvydx*dvxdy) !Physick(1988)
               endif
c     write(*,*) vx(i,j,k),vy(i,j,k),dvxdx,dvydy,dx,dy
            enddo
         enddo
      enddo
c
c.... Scrive kappa_zeta per ogni elemento
c
      nelem = (nx-1)*(ny-1)*(nz-1)
      write(*,*) 'Numero elementi = ',nelem
c
      open(nout,file='kappa2.out',status='unknown')
      write(nout,*) dx, dy
      write(nout,*)
c
      nelem=0
      do i=1,nx-1
         do j=1,ny-1
            do k=1,nz-1
               nelem=nelem+1
               n(1)=k+(j-1)*nz+(i-1)*nz*ny
               n(2)=n(1)+1
               n(3)=n(1)+nz
               n(4)=n(3)+1
               n(5)=n(1)+nz*ny
               n(6)=n(5)+1
               n(7)=n(5)+nz
               n(8)=n(7)+1
               vk=0.d0
               hk=0.d0
               do m=1,8
                  vk = vk+vertk(n(m))
                  hk = hk+hork(n(m))
               enddo
               vk=0.125d0*vk
               hk=0.125d0*hk
               write(nout,*) nelem,vk,hk
            enddo
         enddo
      enddo
c
      close(nout)
      end
