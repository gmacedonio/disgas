      subroutine readwin4(nwin,time,nx,ny,nz,vx,vy)
c.... Read wind field (Real*4 version)
      dimension vx(nx,ny,nz),vy(nx,ny,nz)
c
      read(nwin) time
      read(nwin) nnx,nny,nnz
      read(nwin) dxk,dyk
c
      if(nnx.ne.nx.or.nny.ne.ny.or.nnz+1.ne.nz) then
         write(*,*) '***Error: Incompatible domain dimensions.'
         write(*,*) 'NX=',nnx,' NY=',nny,' NZ=',nnz+1,' in wind file'
         write(*,*) 'NX=',nx,' NY=',ny,' NZ=',nz
         stop
      endif
c
c      write(*,*) 'Grid spacing in diagno.out: DX,DY= ',
c     $     1000.*dxk,1000.*dyk  ! Diagno writes DX,DY in km
c
c.... Zero-level: set Vx=Vy=0
      do j=1,ny
         do i=1,nx
            vx(i,j,1) = 0.d0
            vy(i,j,1) = 0.d0
         enddo
      enddo
c
c.... Read winds from diagno.out (k>1)
      do k=2,nz
         do j=1,ny
            read(nwin) (vx(i,j,k),i=1,nx) ! Read binary file (real*4)
         enddo
      enddo
      do k=2,nz
         do j=1,ny
            read(nwin) (vy(i,j,k),i=1,nx)
         enddo
      enddo
c
      return
      end
