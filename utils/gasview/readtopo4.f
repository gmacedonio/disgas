      subroutine readtopo4(ntpg,topg,nx,ny,tpgmax)
c.... Read topography (real*4 Version)
      dimension topg(nx,ny)
      tpgmax=0.
c
      do j=1,ny
         read(ntpg,*) (topg(i,j),i=1,nx)
         do i=1,nx
            tpgmax=max(topg(i,j),tpgmax)
         enddo
      enddo
      end
