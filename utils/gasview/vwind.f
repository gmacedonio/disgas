      subroutine vwind(vx,vy,nx,ny,nz,work1,work2,n1,n2,iside,level)
      dimension vx(nx,ny,nz),vy(nx,ny,nz)
      dimension work1(n1,n2),work2(n1,n2)
c
      if(iside.eq.0) then
         do i=1,nx
            do j=1,ny
               work1(i,j)=vx(i,j,level)
               work2(i,j)=vy(i,j,level)
            enddo
         enddo
      elseif(iside.eq.1) then
         do i=1,nx
            do k=1,nz
               work1(i,k) = vx(i,level,k)
               work2(i,k) = 0d0
            enddo
         enddo
c
      elseif(iside.eq.2) then
         do j=1,ny
            do k=1,nz
               work1(j,k) = vy(level,j,k)
               work2(j,k) = 0d0
            enddo
         enddo
      endif
c
      end

