#
# Process this file with autoconf to produce a configure script.
#
# To produce files: configure, Makefiles and Scripts
#    $ automake
#    $ autoconf
#
# Eventually, if you receive errors, you need to regenerate
# file aclocal.m4 with the command:
#    $ autoreconf
#  or
#    $ aclocal
#    $ autoconf
#
#
# Force an error if autoconf version is earlier than 2.63 (leave commented)
# AC_PREREQ([2.63])
#
# Autoconf initialization [Package name], [version], [email for problems]
AC_INIT([disgas], [2.6.1], [giovanni.macedonio@ingv.it])
#
# Get git version
AS_IF([test -d .git ], [git describe --always > .gitversion])
AC_SUBST([GIT_VERSION], [`cat .gitversion`])
#
# Allows modification of the program file names (keep commented)
#AC_CANONICAL_TARGET
#AC_ARG_PROGRAM
#
AC_CONFIG_AUX_DIR([autoconf])
# AC_CONFIG_MACRO_DIR([m4])
#
# Automake initialization
AM_INIT_AUTOMAKE
#
# Set language for configuration checks
AC_LANG(Fortran)
AC_FC_SRCEXT(f90)
AC_FC_FREEFORM
#
# Check for Fortran subroutine exit(code)
AC_CHECK_FUNC([exit], [have_exit=yes], [have_exit=no])
AM_CONDITIONAL([HAVE_EXIT], [test x$have_exit = xyes])
#
# Check for Fortran subroutine system(command)
AC_MSG_CHECKING([fot Fortran subroutine system(command)])
AC_LINK_IFELSE([AC_LANG_PROGRAM([], [call system('echo')] )],
[have_system=yes], [have_system=no])
AC_MSG_RESULT([$have_system])
AM_CONDITIONAL([HAVE_SYSTEM], [test x$have_system = xyes])
#
# Check for Fortran command_argument_count()
AC_MSG_CHECKING([for Fortran command_argument_count()])
AC_LINK_IFELSE([AC_LANG_PROGRAM([], [n = command_argument_count()] )],
[have_argument_count=yes], [have_argument_count=no])
AC_MSG_RESULT([$have_argument_count])
AM_CONDITIONAL([HAVE_COMMAND_ARGUMENT_COUNT], [test x$have_argument_count=xyes])
#
# Set the build variables: build_cpu, build_vendor and build_os
# It needs files config.guess and config.sub
AC_CANONICAL_BUILD
#
# After Automake 1.9.6 AS_PROG_MKDIR_P has become obsolete (keep commented)
# AC_PROG_MKDIR_P
#
# Set default prefix (where directory bin is created)
# This is the default top directory of the installation
AC_PREFIX_DEFAULT($HOME)
#
# Scan for possible compilers (used if environment variable FC is not set)
AC_PROG_FC
#
# F90 filename extension
AC_FC_SRCEXT(f90)
#
# Guess default Fortran linker flags
AC_FC_LIBRARY_LDFLAGS
#
# Generate instructions for ar
#m4_pattern_allow([AM_PROG_AR])
#AM_PROG_AR
#
# Generate instructions for ranlib
AC_PROG_RANLIB
#
# Check for existence of makedepf90
AC_CHECK_PROG([MAKEDEPF90], [makedepf90], [makedepf90])

# Message inserted in the output scripts
AC_SUBST([DO_NOT_MODIFY_MESSAGE],
["DO NOT MODIFY (it is generated automatically by configure)"])
#

# List of Makefiles to be processed
AC_CONFIG_FILES([Makefile
		src/Makefile
		src/config.f90
		examples/Makefile
		utils/grd2lw/Makefile
		utils/grd2rough/Makefile
])
#
# Produce all the output
AC_OUTPUT
#
# Write configuration on the screen
AC_MSG_NOTICE([---------------------------------------------------------])
AC_MSG_NOTICE([Configuration complete - $PACKAGE_NAME-$PACKAGE_VERSION])
AC_MSG_NOTICE([Git version: $GIT_VERSION])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Fortran compiler:           FC=$FC])
AC_MSG_NOTICE([Fortran flags:              FCFLAGS=$FCFLAGS])
AC_MSG_NOTICE([Install prefix:             --prefix=$prefix])
AC_MSG_NOTICE([Executables install prefix: --exec_prefix=$exec_prefix])
AC_MSG_NOTICE([Binary directory:           --bindir=$bindir])
AC_MSG_NOTICE([---------------------------------------------------------])
