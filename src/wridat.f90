subroutine wridat
  !*****************************************************************
  !*
  !*   Writes input data to the log file
  !*
  !*****************************************************************
  use inpout
  use master
  implicit none
  !
  integer :: iyr,imo,idy,ihr,imi,ise
  !  integer :: ipts,ibox
  !
  !*** Writes the file
  !
  write(nlog,1)
1 format(/,               &
       '------------',/, &
       ' INPUT DATA ',/, &
       '------------')
  !
  !*** TIME block
  !
  call addtime(ibyr,ibmo,ibdy,ibhr,ibmi,iyr,imo,idy,ihr,imi,ise,trun)

  !
  write(nlog,10) ibdy,month(ibmo),ibyr,ibhr, ibmi*60,    &
       idy,month(imo ),iyr ,ihr ,(imi*60)+ise,&
       trun/36d2,trun
10 format(/,                           &
       'TIME'                    ,/, &
       '  Initial time    : ',2x,i2.2,1x,a3,1x,i4.4,' at ',i2.2,' h ',i4.4,' s',/,&
       '  Final   time    : ',2x,i2.2,1x,a3,1x,i4.4,' at ',i2.2,' h ',i4.4,' s',/,&
       '  Time increment  : ',f6.1,' h (',f12.1,' s)')
  if(.not.restart) then
     write(nlog,11)
  else
     write(nlog,12) time/36d2,time
  end if
11 format(&
       '  Restart         :   No')
12 format(&
       '  Restart from    : ',f6.1,' h (',f12.1,' s)')
  !
  !*** GRID block
  !
  write(nlog,20) x0,y0,x0+(nx-1)*dx,y0+(ny-1)*dy,nx,ny,nz,dx,dy
20 format(/,                                               &
       'GRID',/,                                           &
       '  Bottom-left corner : (',f9.1,2x,f9.1,')',/,      &
       '  Top-right   corner : (',f9.1,2x,f9.1,')',/,      &
       '  Number points x    : ',i9  ,/,                   &
       '  Number points y    : ',i9  ,/,                   &
       '  Number points z    : ',i9  ,/,                   &
       '  X  grid spacing    : ',f9.1,/,                   &
       '  Y  grid spacing    : ',f9.1)
  !
  !*** TOPOGRAPHY block
  !
  write(nlog,30) MINVAL(topg),MAXVAL(topg)
30 format(/,                                               &
       'TOPOGRAPHY',/,                                     &
       '  Min. topography    : ',f9.1,/,                   &
       '  Max. topography    : ',f9.1)
  !
  !*** OUTPUT block
  !
  write(nlog,50) tevery,TRIM(outdir)
50 format(/,                                               &
       'OUTPUT',/,                                         &
       '  Time interval (sec): ',f10.1,/,                  &
       '  Dump directory     : ',a)

  !
  if(out_rst) then
     write(nlog, '(''  Output restart file: yes'')')
  else
     write(nlog, '(''  Output restart file: no'')')
  end if
  !
  if(out_dom) then
     write(nlog,51 ) TRIM(outdir)//'/'//'domain.grd'
  else
     write(nlog,510)
  end if
51 format(&
       '  Output domain      : yes',/, &
       '  Domain file name   : ',a)
510 format(&
       '  Output domain      : no')
  !
  if(out_src) then
     write(nlog,511) TRIM(outdir)//'/'//'source.grd'
  else
     write(nlog,512)
  end if
511 format(&
       '  Output source      : yes',/, &
       '  Source file name   : ',a)
512 format(&
       '  Output source      : no')
  !
  if(out_u) then
     write(nlog,52 )
  else
     write(nlog,520)
  end if
52 format(&
       '  Output u-velocity  : yes')
520 format(&
       '  Output u-velocity  : no ')
  !
  if(out_v) then
     write(nlog,53 )
  else
     write(nlog,530)
  end if
53 format(&
       '  Output v-velocity  : yes')
530 format(&
       '  Output v-velocity  : no ')
  !
  if(out_w) then
     write(nlog,54 )
  else
     write(nlog,540)
  end if
54 format(&
       '  Output w-velocity  : yes')
540 format(&
       '  Output w-velocity  : no ')
  !
  if(out_g) then
     write(nlog,55 )
  else
     write(nlog,550)
  end if
55 format(&
       '  Output ground_load : yes')
550 format(&
       '  Output ground_load : no ')
  !
  if(out_c) then
     write(nlog,570)
  else
     write(nlog,571)
  end if
570 format(&
       '  Output concentr.   : yes')
571 format(&
       '  Output concentr.   : no ')
  !
  !
  !  if(out_pts) then
  !     write(nlog,58 )
  !     do ipts = 1,npts
  !        write(nlog,580) ipts,x_pts(ipts),y_pts(ipts),z_pts(ipts)
  !     end do
  !  else
  !     write(nlog,59)
  !  end if
  ! 58 format('  Track points       : yes')
  ! 580 format('                       Point ',i3,' coordinates ', &
  !          f10.1,1x,f10.1,1x,f7.2)
  ! 59 format('  Track points       : no ')
  !
  ! if(out_box) then
  !   write(nlog,60 )
  !   do ibox= 1,nbox
  !      write(nlog,61) ibox,x_box(ibox),y_box(ibox),z_box(ibox), &
  !           i1_box(ibox),i2_box(ibox),j1_box(ibox),j2_box(ibox)
  !   end do
  ! else
  !   write(nlog,65)
  ! end if
  ! 60 format('  Track boxes        : yes')
  ! 61 format('     Box ',i3,' Coordinates   ',2(f10.1,1x),f7.2,/, &
  !     '             X-points from    ',i3,'  to  ',i3,/,&
  !    '             Y-points from    ',i3,'  to  ',i3)
  ! 65 format('  Track boxes        : no ')
  !
  write(nlog,100)
100 format(/,                &
       '----------------',/, &
       ' TIME EVOLUTION ',/, &
       '----------------',/)
  !
  return
end subroutine wridat
