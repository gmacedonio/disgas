subroutine advctz(c,nx,ny,nz,vz,dz,dt,topg,ztop,work,map)
  !***************************************************************************
  !*
  !*    Advection in the Z direction with non-uniform grid (Lax-Wendroff)
  !*
  !*    c        - Concentration (Input/Output matrix)
  !*    nx,ny,nz - Number of cells in the X,Y,Z directions (Input)
  !*    vz       - Z-component of the wind direction (Input Matrix)
  !*    dz       - Array of sizes of the cells in Z direction (Input)
  !*    dt       - Time step (Input)
  !*    topg     - Topography (Input matrix)
  !*    ztop     - Vertical dimension of the domain (Input)
  !*    work     - Auxiliary vector (Input)
  !*    map      - Apply Z-mapping: 0=NO; 1=YES (Input)
  !*
  !***************************************************************************
  use kindtype
  implicit none
  integer     :: nx,ny,nz,map
  real   (rp) :: dt,ztop,topg(nx,ny)
  real   (rp) :: c(0:nx+1,0:ny+1,0:nz+1),vz(nx,ny,nz)
  real   (rp) :: work(nz+1,2),dz(0:nz),rjac
  !
  integer     :: i,j,k
  real   (rp) :: dzm,vl,vr,vm,Cr,deltar,deltal,sigma,dzl,dzr
  !
  !***  Main loop
  !
  do i=1,nx
     do j=1,ny
        !
        rjac = 1.0_rp  ! Initialize rjac
        if(map==1) rjac = ztop/(ztop-topg(i,j))
        !
        ! k = 1
        dzm = 2.0_rp/(dz(1)+dz(0))
        dzr = 1.0_rp/dz(1)
        dzl = 1.0_rp/dz(0)
        vr = vz(i,j,1)
        vl = vz(i,j,1)
        vm = 0.5_rp*(vl+vr)
        ! First order
        work(1,1) = dt*(vr*c(i,j,1) - vl*c(i,j,0))*dzm
        deltar = (c(i,j,2)-c(i,j,1))*dzr
        deltal = (c(i,j,1)-c(i,j,0))*dzl
        ! sigma = minmod(deltal,deltar)
        sigma  = 0.5_rp*(sign(1.0_rp,deltar) + &
             sign(1.0_rp,deltal))*min(abs(deltar),abs(deltal))
        Cr = abs(vm)*dt*dzm
        work(1,2) = dt*0.5_rp*dz(0)*(1.0_rp-Cr)*sigma*abs(vm)*dzm
        ! k > 1
        do k = 2, nz
           dzm = 2.0_rp/(dz(k)+dz(k-1))
           dzr = 1.0_rp/dz(k)
           dzl = 1.0_rp/dz(k-1)
           vr = vz(i,j,k)
           vl = vz(i,j,k-1)
           vm = 0.5_rp*(vl+vr)
           ! First order
           work(k,1) = dt*(vr*c(i,j,k) - vl*c(i,j,k-1))*dzm
           deltar = (c(i,j,k+1)-c(i,j,k  ))*dzr
           deltal = (c(i,j,k  )-c(i,j,k-1))*dzl
           ! sigma = minmod(deltal,deltar)
           sigma  = 0.5_rp*(sign(1.0_rp,deltar) + &
                sign(1.0_rp,deltal))*min(abs(deltar),abs(deltal))
           Cr = abs(vm)*dt*dzm
           work(k,2) = dt*0.5_rp*dz(k-1)*(1.0_rp-Cr)*sigma*abs(vm)*dzm
           !
        end do
        !
        work(nz+1,1) = dt*vz(i,j,nz)*(c(i,j,nz+1)-c(i,j,nz))/dz(nz)
        work(nz+1,2) = work(nz,2)
        !
        ! k = 1
        if(vz(i,j,1) >= 0.0_rp) then
           c(i,j,1) = c(i,j,1) - rjac*work(1,1)
        else
           c(i,j,1) = c(i,j,1) - rjac*work(2,1) - &
                rjac*(work(2,2)-work(1,2))
        endif
        ! k > 1
        do k = 2, nz
           if(vz(i,j,k) >= 0.0_rp) then
              c(i,j,k) = c(i,j,k) - rjac*work(k,1) - &
                   rjac*(work(k,2)-work(k-1,2))
           else
              c(i,j,k) = c(i,j,k) - rjac*work(k+1,1) - &
                   rjac*(work(k+1,2)-work(k,2))
           endif
        end do
     end do
  end do
  !
  return
end subroutine advctz
