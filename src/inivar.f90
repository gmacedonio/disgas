subroutine inivar
  !****************************************************************************
  !*
  !*     Initializes variables
  !*
  !****************************************************************************
  use master, only: cpu_time_start
  implicit none
  !
  !***   CPU time
  !
  call cpu_time(cpu_time_start)
  !
  return
end subroutine inivar
