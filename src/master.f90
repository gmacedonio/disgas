module master
  use kindtype
  use m_dwm
  implicit none
  save
  !
  ! Parameters
  !
  real(rp),    parameter :: pi = 3.14159265358979_rp   ! Greek PI
  !
  ! Oparation mode
  integer :: disp_type     ! Dispersion type: 0=GAS, 1=PARTICLES
  !
  ! Verbosity
  integer :: verbose       ! Level of verbosity in the log file (0/1)
  !
  ! Output levels
  logical, allocatable :: print_layer(:)  ! Define which layer is printed
  !
  ! Function types
  !
  logical, external :: strcompare
  !
  ! Grid specificactions
  !
  integer  :: nx,ny,nz
  real(8)  :: x0,y0,dx,dy      ! Note: x0,y0,dx,dy are real(8)
  !
  ! Meteo parameters
  !
  character(len=10) :: wind_model,rough_model
  integer  :: th_model,tv_model  ! Horizontal and vertical turbulence model
  real(rp) :: rkh0,rkv0,rlength0
  real(rp) :: pres          ! Atmospheric pressure
  real(rp) :: Tz0           ! Temperature of the soil
  real(rp) :: zref          ! Height of wind measurementents
  real(rp) :: Tzref         ! Temperature at zref
  logical  :: need_ustarmol ! True if ustar and rmonin are not provided (readuni)
  logical  :: need_pres     ! True if pressure is not provided (readdwm)
  real(rp), allocatable :: ustar(:,:)    ! Local ustar
  real(rp), allocatable :: rmonin(:,:)   ! Monin-Obukov Length
  !
  ! The diagno wind field reader
  type(dwm) :: wdiagno
  !
  !***  Vectors with dimensions (1:nx,1:ny,1:nz)
  !
  real(rp), allocatable :: c(:,:,:)                      ! Concentration
  real(rp), allocatable :: vx(:,:,:),vy(:,:,:),vz(:,:,:) ! Wind components
  real(rp), allocatable :: vs(:,:,:)                     ! Settling velocity
  real(rp), allocatable :: rkhor(:,:,:),rkver(:,:,:)     ! Diffusion coeff.
  real(rp), allocatable :: flux(:,:,:)                   ! Source flux
  !
  !
  !***  Vectors with dimensions (1:nx,1:ny)
  !
  real(rp), allocatable :: topg(:,:)     ! Topography
  real(rp), allocatable :: rlength(:,:)  ! Roughness length
  real(rp), allocatable :: cload(:,:)    ! Mass accumulated at the ground
  real(rp), allocatable :: work(:,:)
  !
  !***  Vectors with dimensions (1:nz))
  real(rp), allocatable :: zlayer(:),dz(:)
  !
  !***  Time related variables
  !
  logical  :: restart,resettime
  integer  :: ibyr,ibmo,ibdy,ibhr,ibmi
  integer  :: hflag,rhoflag
  integer  :: icounter
  real(rp) :: tbeg,trun
  !
  !***  Other time related variables
  !
  character(len=3), dimension(12) :: month = (/'JAN','FEB','MAR', &
       'APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC' /)
  real(rp) :: time,dt,meteo_time
  real(rp) :: meteo_time_interval = 120d0
  !
  real :: cpu_time_start,cpu_time_end ! For Intrinsic function cpu_time(REAL)
  !
  integer  :: kfile
  integer  :: map
  real(rp) :: dtc,ztop,tpgmax,wtime
  !
  ! *** Parameters for settling velocity estimation
  !
  real(rp) :: diam    ! Particles diameter
  real(rp) :: rhop    ! Particles density
  real(rp) :: psi     ! Particles sphericity parameter
  integer  :: modv    ! Settling velocity model (see subroutine vsettl)
  !
  !  Total emitted mass from the source
  real(rp) :: totemass = 0.0_rp
  !
CONTAINS
  !
  subroutine getmem
    implicit none
    integer :: istat
    !
    !  Allocates memory for arrays related to nx,ny,nz
    !
    allocate(c(0:nx+1,0:ny+1,0:nz+1),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: c')
    c = 0.0_rp
    !
    allocate(vx(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: vx')
    vx = 0.0_rp
    !
    allocate(vy(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: vy')
    vy = 0.0_rp
    !
    allocate(vz(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: vz')
    vz = 0.0_rp
    !
    allocate(rkhor(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: rkhor')
    rkhor = 0.0_rp
    !
    allocate(rkver(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: rkver')
    rkver = 0.0_rp
    !
    allocate(flux(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: flux')
    flux = 0.0_rp   ! Clear the source
    !
    ! Settling velocity
    allocate(vs(nx,ny,nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: vs')
    vs = 0.0_rp   ! Must be set to zero (overwritten by setvset, when called)
    !
    !  Allocates memory for arrays related to nx,ny
    !
    allocate(topg(nx,ny),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: topg')
    topg = 0.0_rp
    !
    allocate(ustar(nx,ny),STAT=istat)   ! ustar
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: ustar')
    !
    allocate(rmonin(nx,ny),STAT=istat)   ! Monin-Obukov length
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: rmonin')
    !
    allocate(rlength(nx,ny),STAT=istat)   ! Roughness length
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: rlength')
    !
    allocate(cload(nx,ny),STAT=istat)   ! Ground mass load
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: cload')
    cload = 0.0_rp
    !
    !   Allocates memory for arrays related to nz
    !
    ! allocate(zlayer(nz))    ! zlayer and dz are allocated by read_block_grid
    ! zlayer(1:nz) = 0.0_rp   ! because space is needed by read_block_grid to
    ! allocate(dz(0:nz))      ! store z-grid values
    ! if(istat /= 0) call runend(-1,'Cannot allocate memory for vector: dz')
    ! dz(0:nz) = 0.0_rp
    !
    allocate(print_layer(nz),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for print_layer(:)')
    !
    allocate(work(max(nx,ny,nz)+1,2),STAT=istat)
    if(istat /= 0) call runend(-1,'Cannot allocate memory for matrix: work')
    work = 0.0_rp
    !
  end subroutine getmem
  !
end module master
