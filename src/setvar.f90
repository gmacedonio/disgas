subroutine setvar
  !****************************************************************
  !*
  !*   Set the initial conditions on variables (c,vx,vy,vz) as
  !*   constant or as given from a restart file
  !*
  !****************************************************************
  use kindtype
  use inpout, only: nlog,frst,fseqnum
  use master, only: nx,ny,nz,dx,dy,zlayer,x0,y0,vx,vy,vz,c,cload
  use master, only: ibyr,ibmo,ibdy,ibhr,ibmi,time,tbeg,restart,resettime
  use master, only: month
  implicit none
  !
  integer   :: istat
  integer   :: k,nxr,nyr,nzr
  integer   :: ibyrr,ibmor,ibdyr,ibhrr,ibmir,iser
  real(rp)  :: x0r,y0r,dxr,dyr
  real(rp), allocatable :: zlayerr(:)
  !
  if(restart) then
     !
     !*** Restart from a file
     !
     open(90,FILE=TRIM(frst),STATUS='old',form='unformatted',ERR=100)
     !
     read(90) ibyrr,ibmor,ibdyr,ibhrr,ibmir
     !
     read(90) time
     !
     if(resettime) then    ! Restart from time=0 (no-check on initial time)
        tbeg=0.0_rp
     else
        ! Mantain time: in this case check for consistency
        if(ibyrr /= ibyr .or. ibmor /= ibmo .or. ibdyr /= ibdy .or.  &
             ibhrr /= ibhr .or. ibmir /= ibmi ) then
           write(nlog,7) ibdyr,month(ibmor),ibyrr,ibhrr, ibmir
7          format(/,'  TIME in restart file is: ',i2.2,1x,a3,1x,i4.4, &
                ' at ',i2.2,':',i2.2)
           write(nlog,8) ibdy,month(ibmo),ibyr,ibhr, ibmi*60
8          format('  TIME in input   file is: ',i2.2,1x,a3,1x,i4.4, &
                ' at ',i2.2,':',i2.2)
           call runend(-1,      &
                'Incongruous initial TIME in restart file'//TRIM(frst))
        endif
        tbeg = time
     endif
     call addtime(ibyr,ibmo,ibdy,ibhr,ibmi,ibyrr,ibmor,ibdyr,ibhrr,  &
          ibmir,iser,tbeg)
     write(nlog,9) ibdyr,month(ibmor),ibyrr,ibhrr, ibmir,iser
9    format(/,'  RESTART from time : ',i2.2,1x,a3,1x,i4.4, &
          ' at ',i2.2,':',i2.2,':',i2.2)
     !
     read(90) nxr,nyr,nzr
     read(90) dxr,dyr
     !
     allocate (zlayerr(nzr),STAT=istat)
     if(istat/=0) call runend(-1,'setvar: Cannot allocate memory for: zlayerr')
     !
     read(90) zlayerr(1:nzr)
     read(90) x0r,y0r
     !
     read(90) fseqnum   ! Sequence number of output files
     !
     ! Check
     if(nxr /= nx) call runend(-1,'Incongruous nx in restart file'//TRIM(frst))
     if(nyr /= ny) call runend(-1,'Incongruous ny in restart file'//TRIM(frst))
     if(nzr /= nz) call runend(-1,'Incongruous nz in restart file'//TRIM(frst))
     if(x0r /= x0) call runend(-1,'Incongruous x0 in restart file'//TRIM(frst))
     if(y0r /= y0) call runend(-1,'Incongruous x0 in restart file'//TRIM(frst))
     if(dxr /= dx) call runend(-1,'Incongruous dx in restart file'//TRIM(frst))
     if(dyr /= dy) call runend(-1,'Incongruous dy in restart file'//TRIM(frst))
     do k=1,nz
        if(zlayerr(k) /= zlayer(k)) then
           call runend(-1,'Incongruous zlayer in restart file'//TRIM(frst))
        endif
     enddo
     !
     do k=1,nz
        read(90) vx(1:nx,1:ny,k)
        read(90) vy(1:nx,1:ny,k)
        read(90) vz(1:nx,1:ny,k)
        read(90)  c(1:nx,1:ny,k)
     end do
     read(90) cload(1:nx,1:ny)
     !
     deallocate(zlayerr)  ! I don't need it more
     !
     close(90)
     !
  else
     !
     !*** NO restart
     !
     tbeg = 0.0_rp
     !
  end if
  !
  !*** Other initializations
  !
  !
  !*** Prints to the log file
  !
  !  write(nlog,10) MINVAL(vx),MAXVAL(vx),SUM(vx)/(nx*ny), &
  !       MINVAL(vy),MAXVAL(vy),SUM(vy)/(nx*ny*nz), &
  !       MINVAL(vz),MAXVAL(vz),SUM(vz)/(nx*ny*nz), &
  !       MINVAL(c), MAXVAL(c), SUM(c)/(nx*ny*nz),  &
  !       MINVAL(cload), MAXVAL(cload), SUM(cload)/(nx*ny)
  ! 10 format(/,  &
  !       'INITIAL VALUES'          ,/,   &
  !       '                  '      ,/,  &
  !       '  vx    Minimum  :',e12.4,/,  &
  !       '  vx    Maximum  :',e12.4,/,  &
  !       '  vx    Average  :',e12.4,/,  &
  !       '                 '       ,/,  &
  !       '  vy    Minimum  :',e12.4,/,  &
  !       '  vy    Maximum  :',e12.4,/,  &
  !       '  vy    Average  :',e12.4,/,  &
  !       '                 '       ,/,  &
  !       '  vz    Minimum  :',e12.4,/,  &
  !       '  vz    Maximum  :',e12.4,/,  &
  !       '  vz    Average  :',e12.4,/,  &
  !       '                 '       ,/,  &
  !       '  c     Minimum  :',e12.4,/,  &
  !       '  c     Maximum  :',e12.4,/,  &
  !       '  c     Average  :',e12.4,/,  &
  !       '                 '       ,/,  &
  !       '  cload Minimum  :',e12.4,/,  &
  !       '  cload Maximum  :',e12.4,/,  &
  !       '  cload Average  :',e12.4)
  !
  return
  !
  !*** List of errors
  !
100 call runend(-1,'Error opening restart file: '//TRIM(frst))
  !
  return
end subroutine setvar
