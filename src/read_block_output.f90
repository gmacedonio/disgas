subroutine read_block_output
  !****************************************************************
  !*
  !*    Reads OUTPUT BLOCK from the input file
  !*
  !****************************************************************
  use inpout
  use master
  implicit none
  !
  character(len=100) :: message,word
  integer :: istat
  integer :: k,npar
  integer, allocatable :: vlayer(:)
  !
  !   OUTPUT BLOCK
  !
  call get_input_int4(finp,'OUTPUT','LOG_VERBOSITY_LEVEL',verbose,1, &
       istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  ! Output restart file (optional parameter, default yes)
  call get_input_cha(finp,'OUTPUT','OUTPUT_RESTART_FILE',word,1,istat,message)
  if(istat /= 0) then
     ! Flag is missing: use default value
     out_rst = .true.
  else
     if(strcompare(word,'YES')) then
        out_rst = .true.
     elseif(strcompare(word,'NO')) then
        out_rst = .false.
     else
        call runend(-1,'Invalid flag OUTPUT_RESTART_FILE in the input file')
     end if
  end if
  !
  call get_input_cha(finp,'OUTPUT','OUTPUT_GRD_TYPE',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'ASCII')) then
     grd_type = 0
  elseif(strcompare(word,'BINARY')) then
     grd_type = 1
  else
     call runend(-1,'Invalid flag OUTPUT_GRD_TYPE in the input file')
  end if
  !
  call get_input_rea8(finp,'OUTPUT','OUTPUT_INTERVAL_(SEC)',  &
       tevery,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  !  call get_input_cha(finp,'OUTPUT','OUTPUT_DOMAIN',word,1,istat,message)
  !  if(istat /= 0) call runend(-1,message)
  !  if(strcompare(word,'YES')) then
  !     out_dom = .true.
  !  else
  !     out_dom = .false.
  !  end if
  !  !
  !  call get_input_cha(finp,'OUTPUT','OUTPUT_SOURCE',word,1,istat,message)
  !  if(istat /= 0) call runend(-1,message)
  !  if(strcompare(word,'YES')) then
  !     out_src = .true.
  !  else
  !     out_src = .false.
  !  end if
  !
  call get_input_cha(finp,'OUTPUT','OUTPUT_U_VELOCITY',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     out_u = .true.
  elseif(strcompare(word,'NO')) then
     out_u = .false.
  else
     call runend(-1,'Invalid flag OUTPUT_U_VELOCITY in the input file')
  end if
  !
  call get_input_cha(finp,'OUTPUT','OUTPUT_V_VELOCITY',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     out_v = .true.
  elseif(strcompare(word,'NO')) then
     out_v = .false.
  else
     call runend(-1,'Invalid flag OUTPUT_V_VELOCITY in the input file')
  end if
  !
  call get_input_cha(finp,'OUTPUT','OUTPUT_W_VELOCITY',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     out_w = .true.
  elseif(strcompare(word,'NO')) then
     out_w = .false.
  else
     call runend(-1,'Invalid flag OUTPUT_W_VELOCITY in the input file')
  end if
  !
  if(disp_type ==1) then
     call get_input_cha(finp,'OUTPUT','OUTPUT_GROUND_LOAD',word,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     if(strcompare(word,'YES')) then
        out_g = .true.
     elseif(strcompare(word,'NO')) then
        out_g = .false.
     else
        call runend(-1,'Invalid flag OUTPUT_GROUND_LOAD in the input file')
     end if
  else
     out_g=.false.
  endif
  !
  call get_input_cha(finp,'OUTPUT','OUTPUT_CONCENTRATION',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     out_c = .true.
  elseif(strcompare(word,'NO')) then
     out_c = .false.
  else
     call runend(-1,'Invalid flag OUTPUT_CONCENTRATION in the input file')
  end if
  !
  ! Define which layers are printed
  call get_input_npar(finp,'OUTPUT','OUTPUT_LAYERS',npar,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(npar==0) then
     call get_input_cha(finp,'OUTPUT','OUTPUT_LAYERS',word,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     if(strcompare(word,'ALL')) then
        do k=1,nz
           print_layer(k) = .true.
        end do
     else
        message = 'OUTPUT_LAYERS: Invalid argument (ALL or list_of_layers)'
        call runend(-1,message)
     end if
  else
     if(npar > nz) then
        message = 'Number of OUTPUT_LAYERS cannot be greater than NZ'
        call runend(-1,message)
     else
        allocate(vlayer(npar),STAT=istat)
        if(istat /= 0) call runend(-1,'Cannot allocate memory for vlayer')
        !
        call get_input_ivec(finp,'OUTPUT','OUTPUT_LAYERS',vlayer,npar, &
             istat,message)
        print_layer = .false.   ! Clear
        do k=1,npar
           if(vlayer(k) >= 1 .and. vlayer(k) <= nz) then
              print_layer(vlayer(k)) = .true.
           else
              message = 'OUTPUT_LAYERS are out of the range (1:NZ)'
              call runend(-1,message)
           end if
        end do
        deallocate(vlayer)
     endif
  end if
  !
  ! call get_input_cha(finp,'OUTPUT','TRACK_POINTS',word,1,istat,message)
  ! if(istat /= 0) call runend(-1,message)
  ! if(strcompare(word,'YES')) then
  !    out_pts = .true.
  ! else
  !    out_pts = .false.
  ! end if
  ! !
  ! call get_input_cha(finp,'OUTPUT','TRACK_BOXES',word,1,istat,message)
  ! if(istat /= 0) call runend(-1,message)
  ! if(strcompare(word,'YES')) then
  !    out_box = .true.
  ! else
  !    out_box = .false.
  ! end if
  !
  return
end subroutine read_block_output
