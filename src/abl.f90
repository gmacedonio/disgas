subroutine abl(z0,z1,Tz0,Tz1,pres,u,v,ustar,wstar,rmonin)
  !****************************************************************************
  !*
  !*     This routine computes parameters of the ABL
  !*
  !*     INPUTS:
  !*      z0       Roughness length
  !*      z1       Reference level (z1 > z0)
  !*      Tz0      Temperature (K) at ground
  !*      Tz1      Temperature (K) at z1
  !*      pres     Pressure (Pa)
  !*      u        u-velocity (at z1)
  !*      v        v-velocity (at z1)
  !*     OUTPUTS:
  !*      ustar
  !*      wstar    = 0 in this version
  !*      rmonin   Monin-Obukhov length
  !*
  !****************************************************************************
  use kindtype
  implicit none
  !
  real(rp), intent(in)  :: z0,z1,Tz0,Tz1,pres,u,v
  real(rp), intent(out) :: ustar,wstar,rmonin
  !
  real(rp), parameter :: g = 9.81_rp
  real(rp), parameter :: vkarm = 0.4_rp
  real(rp) :: Thz0,Thz1,umod,Ri,Gm,Gh,thstar,coef
  !
  !***   Potential temperature Theta=T*(1000hPa/P))**(R/cp)
  !***   R  =  287  J/kg K   Air specific gas  constant
  !***   cp = 1006  J/kg K   Air specific heat capacity
  !
  Thz0 = Tz0*(1e5_rp/pres)**(0.285_rp)
  Thz1 = Tz1*(1e5_rp/pres)**(0.285_rp)
  !
  !***   Velocity
  !
  umod = max(sqrt(u*u+v*v),1e-6_rp)
  !
  !***   Bulk Richardson number
  ! From Jacobson, 2005, eq.(8.39) with z0_h=z0_m
  !
  Ri = g*(z1-z0)*(Thz1-Thz0)/(umod*umod*Thz0)
  !
  !***   Stable/Unstable ABL
  ! From Jacobson, 2005, eq.(8.41)
  !
  if(Ri > 0.0_rp) then
     ! Stable
     Gm = 1.0_rp/(1.0_rp + 4.7_rp*Ri)**2
     Gh = Gm
  else
     ! Unstable
     coef = vkarm*vkarm*sqrt(abs(Ri)*z1/z0)/log(z1/z0)**2
     Gm = 1.0_rp - 9.4_rp*Ri /(1.0_rp + 70.0_rp*coef)
     Gh = 1.0_rp - 9.4_rp*Ri /(1.0_rp + 50.0_rp*coef)
  end if
  !
  !***   ustar
  ! From Jacobson, 2005, eq.(8.40)
  !
  ustar = vkarm*umod*sqrt(Gm)/log(z1/z0)
  !
  !***   thstar (Pr=1 assumed)
  ! From Jacobson, 2005, eq.(8.40)
  !
  thstar = vkarm*vkarm*umod*(Thz1-Thz0)*Gh/(ustar*log(z1/z0)**2)
  !
  !***   wstar
  !
  wstar = 0.0_rp
  !
  !***   Monin-Obukhov
  !
  ! Avoids division by 0
  if(abs(thstar) > epsilon(1.0_rp)) then  ! Epsilon is a F90 small number
     rmonin = ustar*ustar*Thz0/(vkarm*g*thstar)
  else
     rmonin = ustar*ustar*Thz0/(vkarm*g*sign(epsilon(1.0_rp),thstar))
  end if
  !
  return
end subroutine abl
