subroutine source
  ! Set source
  !
  !   map - Apply Z-mapping: 0=NO; 1=YES (Input)
  !
  use kindtype
  use master
  implicit none
  !
  integer  :: i,j,k
  real(rp) :: fatt,rjac,vol0
  real(rp) :: emass    ! Emitted mass in the time interval
  !
  rjac = 1.0_rp
  fatt = 1.0_rp
  !      if(k==1 .or. k==nz) fatt = 0.5_rp*fatt ! <= @@@@@@@@@
  !
  do j=1,ny
     do i=1,nx
        do k=1,nz
           emass = dt*flux(i,j,k)
           if(k == 1 .and. nz >= 2) then
              vol0 = 0.5_rp*dx*dy*(dz(1)+dz(2))
              ! Assign the mass to the second layer
              c(i,j,2) = c(i,j,2) + emass*rjac/(vol0*fatt)
           else
              vol0=0.5_rp*dx*dy*(dz(k-1)+dz(k))
              c(i,j,k) = c(i,j,k) + emass*rjac/(vol0*fatt)
           end if
           totemass = totemass + emass   ! This is the total emitted mass
        enddo
     enddo
  enddo
  !
  return
end subroutine source
