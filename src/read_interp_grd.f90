subroutine read_interp_grd(fgrd,xo,yo,dx,dy,nx,ny,g)
  !***************************************************************
  !*
  !*   Reads data from a surfer GRD ASCII file and interpolate
  !*
  !***************************************************************
  use kindtype
  use inpout, only: nlog
  use m_interp
  use rwgrd
  implicit none
  !
  character(len=250), intent(in) :: fgrd  ! File containing the input matrix
  integer, intent(in)   :: nx,ny     ! Number of pixels in the output matrix
  real(rp), intent(in)  :: xo,yo     ! Coordinates of the bottom-left corner
  real(rp), intent(in)  :: dx,dy     ! Pixel size of the output matrix
  real(rp), intent(out) :: g(nx,ny)  ! Output matrix
  integer  :: ixgrd,iygrd
  real(rp) :: dxgrd,dygrd
  real(rp) :: xf,yf         ! Coordinates of the upper-right corner
  integer  :: istat
  !
  ! Types for GRD file
  character(len=4) :: magic
  integer(kind=2)  :: nxgrd2,nygrd2
  real(kind=8)     :: x1grd8,y1grd8,x2grd8,y2grd8
  real(kind=8)     :: zogrd8,zfgrd8
  real(kind=4), allocatable :: grd4(:,:)   ! Temporary variable
  !
  ! New type for GRD matrix
  integer               :: nxgrd,nygrd
  real(rp)              :: x1grd,y1grd  ! Bottom-left center pixel
  real(rp)              :: x2grd,y2grd  ! Upper-right center pixel
  real(rp)              :: xogrd,yogrd  ! Bottom-left corner
  real(rp)              :: xfgrd,yfgrd  ! Upper-right corner
  real(rp), allocatable :: grd(:,:)     ! Temporary variable
  !
  ! Other temporary variables
  integer, allocatable  :: xindex(:),yindex(:)
  real(rp), allocatable :: xweight(:),yweight(:)
  !
  !*** Computes xf and yf
  !
  xf = xo + nx*dx
  yf = yo + ny*dy
  !
  !*** Reads the file (ASCII version)
  !
  open(90,FILE=TRIM(fgrd),STATUS='old',err=100)
  read(90,'(a4)',err=101) magic
  if(TRIM(magic) /= 'DSAA') call runend(-1,'GRD file not in ASCII format')
  read(90,*,err=101) nxgrd2,nygrd2   ! These are integer*2 (in binary files)
  read(90,*,err=101) x1grd8,x2grd8   ! X-domain: centers of the cells
  read(90,*,err=101) y1grd8,y2grd8   ! Y-domain: centers of the cells
  read(90,*,err=101) zogrd8,zfgrd8
  !
  nxgrd = nxgrd2    ! Convert to integer*4
  nygrd = nygrd2
  x1grd = x1grd8    ! Convert to real(rp), center of the cell
  y1grd = y1grd8
  x2grd = x2grd8    ! Convert to real(rp), center of the cell
  y2grd = y2grd8
  !
  dxgrd = (x2grd-x1grd)/(nxgrd-1)
  dygrd = (y2grd-y1grd)/(nygrd-1)
  !
  ! Corners
  xogrd = x1grd - 0.5_rp*dxgrd
  xfgrd = x2grd + 0.5_rp*dxgrd
  yogrd = y1grd - 0.5_rp*dygrd
  yfgrd = y2grd + 0.5_rp*dygrd
  !
  !*** Checks if the computational domain is within the regional GRD file
  !
  if(xogrd > xo) call runend(-1,'xo of the domain is outside the GRD file')
  if(xfgrd < xf) call runend(-1,'xf of the domain is outside the GRD file')
  if(yogrd > yo) call runend(-1,'yo of the domain is outside the GRD file')
  if(yfgrd < yf) call runend(-1,'yf of the domain is outside the GRD file')
  !
  allocate(grd4(nxgrd,nygrd),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for grd4')
  !
  do iygrd=1,nygrd
     read(90,*,err=101) (grd4(ixgrd,iygrd),ixgrd=1,nxgrd) ! Read GRD matrix
  end do
  !
  close(90)
  !
  !*** Interpolates
  !
  allocate(xindex(nx),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for xindex')
  allocate(yindex(ny),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for yindex')
  allocate(xweight(nx),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for xweight')
  allocate(yweight(ny),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for yweight')
  !
  ! Allocate memory for grd (real(rp))
  allocate(grd(nxgrd,nygrd),STAT=istat)
  if(istat /= 0) call runend(-1,'Cannot allocate memory for grd')
  !
  grd = grd4                 ! Convert to real(rp)
  !
  ! Find interpolation weights and indexes
  call find_weights_uniform(x1grd,nxgrd,dxgrd,xo+0.5*dx,nx,dx,xindex,xweight,istat)
  if(istat /= 0) then
     write(nlog,*) '***Error (X): ',istat
     call runend(-1,'Error in subroutine find_weight_uniform')
  end if
  !
  call find_weights_uniform(y1grd,nygrd,dygrd,yo+0.5*dy,ny,dy,yindex,yweight,istat)
  if(istat /= 0) then
     write(nlog,*) '***Error (Y): ',istat
     call runend(-1,'Error in subroutine find_weight_uniform')
  end if
  !
  ! Perform interpolation
  call interpolate_2d(nxgrd,nygrd,grd,nx,ny,g,xindex,xweight,yindex,yweight)
  !
  !*** Deallocate memory
  !
  deallocate(xindex)
  deallocate(yindex)
  deallocate(xweight)
  deallocate(yweight)
  deallocate(grd)
  deallocate(grd4)           ! Release memory for grd4
  !
  return
  !
  !*** List of errors
  !
100 call runend(-1,'Error opening GRD file: '//TRIM(fgrd))
101 call runend(-1,'Error reading GRD file: '//TRIM(fgrd))
  return
  !
end subroutine read_interp_grd
