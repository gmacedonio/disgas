subroutine setup
  !**************************************************************************
  !*
  !*    Sets up the run
  !*
  !*    Date: 17-NOV-2023
  !*
  !**************************************************************************
  use kindtype
  use config
  use inpout
  use master
  implicit none
  !
  integer           :: narg
  character(len=10) :: stime
  character(len=8 ) :: sdate
  !
  !***  Gets input and log filenames from the call arguments
  !
  narg = command_argument_count()   ! Number of arguments
  if(narg /=1 .and. narg /= 2) then
     write(*,'(''disgas '',a,'' ('',a,'')'')') trim(version),trim(git_version)
     write(*,'(''Usage: disgas input_file [log_file]'')')
     call exit(1)
  endif
  !
  call get_command_argument(1,finp)    ! Input file
  !
  if(narg ==2) then
     call get_command_argument(2,flog)    ! Log file
     !
     ! Opens the log file
     open (nlog,file=TRIM(flog),status='unknown')
     !
  else
     ! Use the terminal
     nlog = 6
  endif
  !
  call DATE_AND_TIME(sdate,stime)
  !
  write(nlog,1) trim(version), trim(git_version), sdate(7:8), sdate(5:6), &
       sdate(1:4), stime(1:2), stime(3:4), stime(5:6)
1 format(/,                         &
       '---------------------------------------',/, &
       ' DISGAS PROGRAM version ',a,' (',a,')',/, &
       '---------------------------------------',/, &
       '                ',/, &
       '  Starting date ',a2,'-',a2,'-',a4,' at time ',a2,':',a2,':',a2)
  !
  !***  Initializes variables
  !
  call inivar
  !
  !***  Reads TIME BLOCK from input file
  !
  call read_block_time
  !
  !***  Reads FILES BLOCKS from input file
  !
  call read_block_files
  !
  !***  Reads GRID BLOCK from input file
  !
  call read_block_grid
  !
  !***  Reads PROPERTIES BLOCK from input file
  !
  call read_block_properties
  !
  ! ***   Allocates memory
  !
  call getmem
  !
  !***  Reads PROPERTIES BLOCK from input file
  !
  !    call read_block_pro
  !
  !***  Reads METEO BLOCK from input file
  !
  call read_block_meteo
  !
  !***  Reads OUTPUT BLOCK from input file
  !
  call read_block_output
  !
  !***  Reads NUMERIC BLOCK from input file
  !
  !  call read_block_num
  !
  !***  Read BLOCK TOPOGRAPHY from input file
  !
  call read_block_topo
  !
  tpgmax = MAXVAL(topg)
  ztop=tpgmax+zlayer(nz)
  !
  !***  If necessary, reads the tracking points file
  !
  !  if(out_pts) then
  !     call setpts
  !  else
  !     npts = 0
  !  end if
  !
  !***  If necessary, reads the tracking boxes file
  !
  !  if(out_box) then
  !     call setbox
  !  else
  !     nbox = 0
  !  end if
  !
  !***  Set the initial conditions on variables as constant or
  !***  as given from a restart file
  !
  call setvar
  !
  !***  Reads the source file
  !
  call setsrc
  !
  !***  Writes input data
  !
  call wridat
  return
end subroutine setup
