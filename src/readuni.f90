subroutine readuni
  !***************************************************************************
  !*
  !*  This routine reads meteorological data from a meteo
  !*  file at current time
  !*
  !*  OUTPUTS
  !*     meteo_time (input/output)
  !*     vx,vy,ustar
  !*
  !*  METEO FILE FORMATS
  !*    HEADER (two records)
  !*          xsta,ysta,zref
  !*          iyr,imo,idy,ihr,imi,code
  !*
  !*    CUP   : time1(s) time2(s) windx(m/s) windy(m/s) Tz0(C) Tzref(C) P(hPa)
  !*    SONIC : time1(s) time2(s) windx(m/s) windy(m/s) Tzref(C) ustar0 rmonin
  !*
  !***************************************************************************
  use kindtype
  use inpout, only: fmet, nlog
  use master, only: nx,ny,x0,y0,dx,dy,zlayer
  use master, only: ibyr,ibmo,ibdy,ibhr,ibmi,month,time,strcompare
  use master, only: meteo_time,vx,vy,ustar,need_ustarmol,need_pres
  use master, only: zref,Tzref,Tz0,pres,rmonin
  implicit none
  !
  integer  :: iyr,imo,idy,ihr,imi
  real(rp) :: time1,time2
  real(rp) :: windu_uni,windv_uni,ustar_uni,rmonin_uni
  real(rp) :: Tz0_uni,Tzref_uni,pres_uni
  real(rp) :: wstar
  real(rp) :: xsta,ysta       ! Coordinates of the meteo station
  character(len =10) :: code
  !
  !***  Reads the file header in the form:
  !       XSTA YSTA ZREF         # Coordinates X,Y, Z (above the ground))
  !       YYYY MM DD HH MM code  # Date, time, code of the station (CUP/SONIC)
  !
  open(99,file=trim(fmet),status='old',err=100)
  !
  ! Read file header (both CUP and SONIC)
  read(99,*,err=101) xsta,ysta,zref
  !
  ! Check station location
  if(xsta < x0 .or. xsta > x0+(nx-1)*dx .or. &
       ysta < y0 .or. ysta > y0+(ny-1)*dy) then
     call runend(-1,'Wind station is outside of the computational domain')
  endif
  if(zref > maxval(zlayer)) call runend(-1,'Wind station Zref > max(zlayer)')
  !
  read(99,*,err=101) iyr,imo,idy,ihr,imi,code
  !
  if(iyr /= ibyr .or. imo /= ibmo .or. idy /= ibdy .or. ihr /= ibhr &
       .or. imi /= ibmi) then
     write(nlog,10) idy,month(imo),iyr,ihr,imi,ibdy,month(ibmo),ibyr,ibhr,ibmi
10   format(' ERROR:',/, &
          ' Initial time in wind  file : ', &
          i2.2,1x,a3,1x,i4.4,' at ',i2.2,':',i2.2,/, &
          ' Initial time in input file : ',i2.2,1x,a3,1x,i4.4, &
          ' at ', i2.2,':',i2.2)
     call runend(-1,'Incongruous initial time in meteo and input files')
  endif
  !
  if(strcompare(code,'CUP')) then
     !
     !***  CUP anemometer
     !
     do while(.true.)
        !
        read(99,*,end=102,err=101) time1,time2,  &
             windu_uni,windv_uni,Tz0_uni,Tzref_uni,pres_uni
        !
        if( time >= time1 .and. time <= time2) then
           meteo_time = time2
           pres_uni  = pres_uni*100.0_rp             ! HPa --> Pa
           Tz0_uni   = Tz0_uni   + 273.16_rp         ! C  --> K
           Tzref_uni = Tzref_uni + 273.16_rp         ! C  --> K
           exit   ! Exit from loop
        end if
     end do
     !
     ! Need ustar
     ! Provides Zref, Tz0, Tzref, pres, u, v for subroutine abl
     !
     Tz0 = Tz0_uni
     Tzref = Tzref_uni
     pres = pres_uni
     need_pres     = .false.
     need_ustarmol = .true.
     !
  elseif(strcompare(code,'SONIC')) then
     !
     !***  SONIC anemometer
     !
     do while(.true.)
        !
        read(99,*,end=102,err=101) time1,time2,  &
             windu_uni,windv_uni,Tzref,ustar_uni,rmonin_uni
        !
        if(time >= time1 .and. time <= time2) then
           meteo_time = time2
           Tzref  = Tzref + 273.16_rp              ! C  --> K
           exit   ! Exit from loop
        end if
     enddo
     !
     ! Set global matrix ustar and rmonin
     need_pres     = .true.
     need_ustarmol = .false.
     ustar  = ustar_uni   ! Copy scalar to matrix
     rmonin = rmonin_uni  ! Copy scalar to matrix
     !
  else
     call runend(-1,'Invalid anemometer code in meteo file')
  endif
  !
  !***  Assigns uniform values to the arrays
  !
  vx     = windu_uni  ! Matrix
  vy     = windv_uni  ! Matrix
  wstar  = 0.0_rp     ! Scalar
  !
  close(99)
  return
  !
  !***  List of errors
  !
100 call runend(-1,'Error opening file: '//TRIM(fmet))
101 call runend(-1,'Error reading file: '//TRIM(fmet))
102 call runend(-1,'Premature end of file: '//TRIM(fmet))
  return
end subroutine readuni
