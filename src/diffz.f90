subroutine diffz(c,nx,ny,nz,dz,dt,rkver,topg,ztop,map)
  ! Diffusion (Z)
  !
  ! map - Apply Z-mapping: 0=NO; 1=YES (Input)
  !
  use kindtype
  implicit none
  !
  integer, intent(in) :: nx,ny,nz,map
  real(rp), intent(in) :: rkver(nx,ny,nz)
  real(rp), intent(inout) :: c(0:nx+1,0:ny+1,0:nz+1)
  real(rp), intent(in) :: dt,dz(0:nz),topg(nx,ny),ztop
  !
  real(rp), parameter :: D8MIN=1e-13_rp
  real(rp) :: dzeta2,ddz1,ddz2,rkz1,rkz2,rks1,rks2,zjac2
  integer :: i,j,k,km1,kp1
  logical :: arithmetic_mean = .true.
  !
  zjac2 = 1.0_rp
  !
  do j=1,ny
     do i=1,nx
        !
        ! Jacobian for following terrain zjac=ztop/(ztop-topg(i,j))
        if(map==1) zjac2 = (ztop/(ztop-topg(i,j)))**2
        !
        do k=1,nz
           km1=max(1,k-1)
           kp1=min(k+1,nz)
           dzeta2=0.5_rp*(dz(k-1)+dz(k))
           ddz1=dz(k-1)*dzeta2
           ddz2=dz(k)*dzeta2
           !
           rks1=0.5_rp*(rkver(i,j,km1)+rkver(i,j,k))
           rks2=0.5_rp*(rkver(i,j,k)+rkver(i,j,kp1))
           !
           if(arithmetic_mean) then
              ! Aritmetic mean
              rkz1=rks1/ddz1
              rkz2=rks2/ddz2
           else
              ! Geometric mean
              if(rks1 > D8MIN) then
                 rkz1=(rkver(i,j,km1)*rkver(i,j,k))/(rks1*ddz1)
              else
                 rkz1=0.0_rp
              endif
              if(rks2 > D8MIN) then
                 rkz2=(rkver(i,j,k)*rkver(i,j,kp1))/(rks2*ddz2)
              else
                 rkz2=0.0_rp
              endif
           endif
           !
           c(i,j,k) = c(i,j,k) + dt*zjac2*(rkz1*c(i,j,k-1) - &
                (rkz1+rkz2)*c(i,j,k) + rkz2*c(i,j,k+1))
        enddo
     enddo
  enddo
  return
end subroutine diffz
