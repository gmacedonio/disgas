module m_interp
  !
  ! Tools for the interpolation of vectors and matrices
  !
  ! Author: Giovanni Macedonio
  ! INGV - Osservatorio Vesuviano, Napoli, Italy
  ! e-mail: giovanni.macedonio@ingv.it
  !
  ! Version: 1.6
  !
  ! Date of first version: 12-SEP-2010
  ! Date of this  version: 11-DEC-2022
  !
  ! Note: Vector/Matrix values are defined at the pixel centers
  !
  implicit none

  interface resample_matrix
     module procedure resample_matrix_2d_r4
     module procedure resample_matrix_2d_r8
  end interface resample_matrix

  interface pick_matrix
     module procedure pick_matrix_2d_r4
     module procedure pick_matrix_2d_r8
  end interface pick_matrix

  interface find_weights_uniform
     module procedure find_weights_uniform_single_r4
     module procedure find_weights_uniform_single_r8
     module procedure find_weights_uniform_many_r4
     module procedure find_weights_uniform_many_r8
  end interface find_weights_uniform

  interface find_weights_nonuniform
     module procedure find_weights_nonuniform_single_r4
     module procedure find_weights_nonuniform_single_r8
     module procedure find_weights_nonuniform_many_r4
     module procedure find_weights_nonuniform_many_r8
  end interface find_weights_nonuniform

  interface interpolate_1d
     module procedure interpolate_1d_single_r4
     module procedure interpolate_1d_single_r8
     module procedure interpolate_1d_many_r4
     module procedure interpolate_1d_many_r8
  end interface interpolate_1d

  interface interpolate_2d
     module procedure interpolate_2d_single_r4
     module procedure interpolate_2d_single_r8
     module procedure interpolate_2d_many_r4
     module procedure interpolate_2d_many_r8
  end interface interpolate_2d

  interface interpolate_3d
     module procedure interpolate_3d_many_r4
     module procedure interpolate_3d_many_r8
  end interface interpolate_3d

contains

  !
  ! Pick matrix value and resample matrix
  !
  subroutine pick_matrix_2d_r4(mat,nx,ny,xmin,ymin,dx,dy,x,y,val,status)
    ! Pick a value in the matrix
    integer, parameter    :: rp=kind(1.0)
    integer, intent(in)   :: nx,ny
    integer, intent(out)  :: status
    real(rp), intent(in)  :: mat(:,:)
    real(rp), intent(in)  :: xmin,ymin   ! Bottom-left pixel center coordinates
    real(rp), intent(in)  :: dx,dy,x,y
    real(rp), intent(out) :: val
    integer  :: indx,indy
    real(rp) :: wgtx,wgty
    ! Find index and weight (x)
    call find_weights_uniform(xmin,nx,dx,x,indx,wgtx,status)
    if(status /= 0) then
       status = 1
       return
    end if
    ! Find index and weight (y)
    call find_weights_uniform(ymin,ny,dy,y,indy,wgty,status)
    if(status /= 0) then
       status = 2
       return
    end if
    ! Interpolate
    call interpolate_2d(nx,ny,mat,val,indx,wgtx,indy,wgty)
  end subroutine pick_matrix_2d_r4

  subroutine pick_matrix_2d_r8(mat,nx,ny,xmin,ymin,dx,dy,x,y,val,status)
    ! Pick a value in the matrix
    integer, parameter    :: rp=kind(1d0)
    integer, intent(in)   :: nx,ny
    integer, intent(out)  :: status
    real(rp), intent(in)  :: mat(:,:)
    real(rp), intent(in)  :: xmin,ymin   ! Bottom-left pixel center coordinates
    real(rp), intent(in)  :: dx,dy,x,y
    real(rp), intent(out) :: val
    integer  :: indx,indy
    real(rp) :: wgtx,wgty
    ! Find index and weight (x)
    call find_weights_uniform(xmin,nx,dx,x,indx,wgtx,status)
    if(status /= 0) then
       status = 1
       return
    end if
    ! Find index and weight (y)
    call find_weights_uniform(ymin,ny,dy,y,indy,wgty,status)
    if(status /= 0) then
       status = 2
       return
    end if
    ! Interpolate
    call interpolate_2d(nx,ny,mat,val,indx,wgtx,indy,wgty)
  end subroutine pick_matrix_2d_r8

  subroutine resample_matrix_2d_r4(imat,inx,iny,ixmin,iymin,idx,idy, &
       omat,onx,ony,oxmin,oymin,odx,ody,status)
    ! Resample a matrix
    ! Combines find_weights and interpolate 2d
    integer,  parameter   :: rp=kind(1.0)  ! Set the real precision
    real(rp), intent(in)  :: imat(:,:)
    integer,  intent(in)  :: inx,iny
    real(rp), intent(in)  :: ixmin,iymin  ! Bottom-left pixel center coordinates
    real(rp), intent(in)  :: idx,idy
    real(rp), intent(out) :: omat(:,:)    ! Must be allocated
    integer,  intent(out) :: onx,ony
    real(rp), intent(out) :: oxmin,oymin  ! Bottom-left pixel center coordinates
    real(rp), intent(out) :: odx,ody
    integer,  intent(out) :: status
    integer,  allocatable :: indx(:),indy(:)
    real(rp), allocatable :: wgtx(:),wgty(:)
    ! Allocate temporary memory
    allocate(indx(onx))
    allocate(indy(ony))
    allocate(wgtx(onx))
    allocate(wgty(onx))
    ! Find weights (x)
    call find_weights_uniform(ixmin,inx,idx,oxmin,onx,odx,indx,wgtx,status)
    if(status /= 0) then
       status = 1
       return
    end if
    ! Find weights (y)
    call find_weights_uniform(iymin,iny,idy,oymin,ony,ody,indy,wgty,status)
    if(status /= 0) then
       status = 2
       return
    end if
    ! Interpolate
    call interpolate_2d(inx,iny,imat,onx,ony,omat,indx,wgtx,indy,wgty)
    ! Free memory
    deallocate(indx)
    deallocate(indy)
    deallocate(wgtx)
    deallocate(wgty)
  end subroutine resample_matrix_2d_r4

  subroutine resample_matrix_2d_r8(imat,inx,iny,ixmin,iymin,idx,idy, &
       omat,onx,ony,oxmin,oymin,odx,ody,status)
    ! Resample a matrix
    ! Combines find_weights and interpolate 2d
    integer,  parameter   :: rp=kind(1d0)  ! Set the real precision
    real(rp), intent(in)  :: imat(:,:)
    integer,  intent(in)  :: inx,iny
    real(rp), intent(in)  :: ixmin,iymin  ! Bottom-left pixel center coordinates
    real(rp), intent(in)  :: idx,idy
    real(rp), intent(out) :: omat(:,:)    ! Must be allocated
    integer,  intent(out) :: onx,ony
    real(rp), intent(out) :: oxmin,oymin  ! Bottom-left pixel center coordinates
    real(rp), intent(out) :: odx,ody
    integer,  intent(out) :: status
    integer,  allocatable :: indx(:),indy(:)
    real(rp), allocatable :: wgtx(:),wgty(:)
    ! Allocate temporary memory
    allocate(indx(onx))
    allocate(indy(ony))
    allocate(wgtx(onx))
    allocate(wgty(onx))
    ! Find weights (x)
    call find_weights_uniform(ixmin,inx,idx,oxmin,onx,odx,indx,wgtx,status)
    if(status /= 0) then
       status = 1
       return
    end if
    ! Find weights (y)
    call find_weights_uniform(iymin,iny,idy,oymin,ony,ody,indy,wgty,status)
    if(status /= 0) then
       status = 2
       return
    end if
    ! Interpolate
    call interpolate_2d(inx,iny,imat,onx,ony,omat,indx,wgtx,indy,wgty)
    ! Free memory
    deallocate(indx)
    deallocate(indy)
    deallocate(wgtx)
    deallocate(wgty)
  end subroutine resample_matrix_2d_r8

  !
  ! Find index and weight
  !
  subroutine find_weights_uniform_single_r4(x0,nx,dx,xdest,ind,weight,status)
    ! Find the weights for linear interpolation
    ! NOTE: x0 is the coordinate of the center of the left pixel
    ! x(i) = x0 +(i-1)*dx, i=1,nx  are the pixel centers
    ! If y(1:nx) are the source vector values, then
    ! ydest = y(ind)*weight + y(ind+1)*(1-weight)
    implicit none
    integer, parameter    :: rp=kind(1.0)  ! Set the real precision
    real(rp), intent(in)  :: x0       ! Coordinate of the left pixel center
    real(rp), intent(in)  :: dx       ! Grid spaging
    integer,  intent(in)  :: nx       ! Number of grid elements
    real(rp), intent(in)  :: xdest    ! X-value
    integer,  intent(out) :: ind      ! Output index
    real(rp), intent(out) :: weight   ! Weight
    integer,  intent(out) :: status   ! Exit status code
    real(rp) :: xmax, pos
    !
    status = 0                        ! Default: OK
    !
    ! Check arguments
    if(nx <= 1) then
       status = status + 1            ! Invalid nx
    endif
    if(dx <= 0.0_rp) then
       status = status + 2            ! Invalid dx
    endif
    ! Return on error
    if(status /= 0) return
    !
    xmax = x0 + (nx-1)*dx             ! Coordinate of center of right pixel
    ! Extrapolate out of boundaries
    if(xdest < x0) then
       ind = 1
       weight = 1.0_rp
       return
    end if
    if(xdest > xmax) then
       ind = nx-1
       weight = 0.0_rp
       return
    end if
    !
    ! Find the weight
    pos = (xdest-x0)/dx
    ind = min(int(pos)+1, nx-1)
    weight = ind - pos
  end subroutine find_weights_uniform_single_r4

  subroutine find_weights_uniform_single_r8(x0,nx,dx,xdest,ind,weight,status)
    ! Find the weights for linear interpolation
    ! NOTE: x0 is the coordinate of the center of the left pixel
    ! x(i) = x0 +(i-1)*dx, i=1,nx  are the pixel centers
    ! If y(1:nx) are the source vector values, then
    ! ydest = y(ind)*weight + y(ind+1)*(1-weight)
    implicit none
    integer, parameter    :: rp=kind(1d0)  ! Set the real precision
    real(rp), intent(in)  :: x0       ! Coordinate of the left pixel center
    real(rp), intent(in)  :: dx       ! Grid spaging
    integer,  intent(in)  :: nx       ! Number of grid elements
    real(rp), intent(in)  :: xdest    ! X-value
    integer,  intent(out) :: ind      ! Output index
    real(rp), intent(out) :: weight   ! Weight
    integer,  intent(out) :: status   ! Exit status code
    real(rp) :: xmax, pos
    !
    status = 0                        ! Default: OK
    ! Check arguments
    if(nx <= 1) then
       status = status + 1            ! Invalid nx
    endif
    if(dx <= 0.0_rp) then
       status = status + 2            ! Invalid dx
    endif
    ! Return on error
    if(status /= 0) return
    !
    xmax = x0 + (nx-1)*dx             ! Coordinate of center of right pixel
    !
    ! Extrapolate out of boundaries
    if(xdest < x0) then
       ind = 1
       weight = 1.0_rp
       return
    end if
    if(xdest > xmax) then
       ind = nx-1
       weight = 0.0_rp
       return
    end if
    !
    ! Find the weight
    pos = (xdest-x0)/dx
    ind = min(int(pos)+1, nx-1)
    weight = ind - pos
  end subroutine find_weights_uniform_single_r8

  !
  ! Find multiple ind and weights
  !
  subroutine find_weights_uniform_many_r4(x0src,nxsrc,dxsrc,x0dest,nxdest, &
       dxdest,ind,weight,status)
    ! Find the weights for linear interpolation
    ! NOTE: x0src is the coordinate of the center of the left pixel
    ! xsrc(1:nxsrc) are the positions of the center of the pixels of the source
    ! vector where xsrc(i)=x0src+(i-1)*dxsrc, i=1,nxsrc are the pixel centers
    ! xdest(1:nxdest) are the positions of the center of the pixels of the
    ! destination vector where xdest(i) = x0dest+(i-1)*dxdest, i=1,nxdest
    ! are the pixel centers
    ! If y(1:nx) are the source vector values, then for j=1,nxdest
    ! ydest(j) = y(ind(j))*weight(j) + y(ind(j)+1)*(1-weight(j))
    implicit none
    integer, parameter    ::rp=kind(1.0)     ! Set the real precision
    real(rp), intent(in)  :: x0src,x0dest    ! Centers of the left pixels
    real(rp), intent(in)  :: dxsrc,dxdest
    integer,  intent(in)  :: nxsrc,nxdest
    integer,  intent(out) :: ind(nxdest)
    real(rp), intent(out) :: weight(nxdest)
    integer,  intent(out) :: status          ! Exit status code
    !
    ! Local variables
    integer  :: i
    real(rp) :: x2

    status = 0   ! Default: OK

    ! Check arguments
    if(nxsrc <= 1 .or. nxdest <= 0) then
       status = status + 1            ! Invalid nxsrc or nxdest
    endif
    if(dxsrc <= 0.0_rp .or. dxdest <= 0.0_rp) then
       status = status + 2            ! Invalid dxsrc or dxdest
    endif

    ! Return on error
    if(status /= 0) return

    ! Evaluate weights
    do i=1,nxdest
       x2 = x0dest+(i-1)*dxdest
       call find_weights_uniform(x0src, nxsrc, dxsrc, x2, ind(i), weight(i), status)
       if(status /= 0) return
    end do
  end subroutine find_weights_uniform_many_r4

  subroutine find_weights_uniform_many_r8(x0src,nxsrc,dxsrc,x0dest,nxdest, &
       dxdest,ind,weight,status)
    ! Find the weights for linear interpolation
    ! NOTE: x0src is the coordinate of the center of the left pixel
    ! xsrc(1:nxsrc) are the positions of the center of the pixels of the source
    ! vector where xsrc(i)=x0src+(i-1)*dxsrc, i=1,nxsrc are the pixel centers
    ! xdest(1:nxdest) are the positions of the center of the pixels of the
    ! destination vector where xdest(i) = x0dest+(i-1)*dxdest, i=1,nxdest
    ! are the pixel centers
    ! If y(1:nx) are the source vector values, then for j=1,nxdest
    ! ydest(j) = y(ind(j))*weight(j) + y(ind(j)+1)*(1-weight(j))
    implicit none
    integer, parameter    ::rp=kind(1d0)     ! Set the real precision
    real(rp), intent(in)  :: x0src,x0dest    ! Centers of the left pixels
    real(rp), intent(in)  :: dxsrc,dxdest
    integer,  intent(in)  :: nxsrc,nxdest
    integer,  intent(out) :: ind(nxdest)
    real(rp), intent(out) :: weight(nxdest)
    integer,  intent(out) :: status          ! Exit status code
    !
    ! Local variables
    integer  :: i
    real(rp) :: x2

    status = 0   ! Default: OK

    ! Check arguments
    if(nxsrc <= 1 .or. nxdest <= 0) then
       status = status + 1            ! Invalid nxsrc or nxdest
    endif
    if(dxsrc <= 0.0_rp .or. dxdest <= 0.0_rp) then
       status = status + 2            ! Invalid dxsrc or dxdest
    endif

    ! Return on error
    if(status /= 0) return

    ! Evaluate weights
    do i=1,nxdest
       x2 = x0dest+(i-1)*dxdest
       call find_weights_uniform(x0src, nxsrc, dxsrc, x2, ind(i), weight(i), status)
       if(status /= 0) return
    end do
  end subroutine find_weights_uniform_many_r8

  subroutine find_weights_nonuniform_single_r4(xsrc, nx, xdest, ind, weight, status)
    ! Find the weight for linear interpolation
    ! NOTE: xsrc(1:nx) contains the coordinates of the pixel centers
    ! If y(1:nx) are the source vector values, then
    ! ydest = y(ind)*weight + y(ind+1)*(1-weight)
    implicit none
    integer, parameter :: rp=kind(1.0)
    integer,  intent(in)  :: nx          ! Number of vector elements
    real(rp), intent(in)  :: xsrc(nx)    ! Coordinates of the cell centers
    real(rp), intent(in)  :: xdest
    integer,  intent(out) :: ind
    real(rp), intent(out) :: weight
    integer,  intent(out) :: status      ! Exit status code
    integer  :: ir

    status = 0     ! Default: OK

    ! Extrapolate out of boundaries
    if(xdest < xsrc(1)) then
       ind = 1
       weight = 1.0_rp
       return
    end if
    if(xdest > xsrc(nx)) then
       ind = nx-1
       weight = 0.0_rp
       return
    end if

    ! Check arguments
    if(nx <= 1) then
       status = status + 1            ! Invalid nxsrc
    endif

    ! Return on error
    if(status /= 0) return

    ! Divide and conquer method
    ir=nx
    ind=1
    do while(ind+1 /= ir)
       if(xdest < xsrc((ind+ir)/2)) then
          ir = (ind+ir)/2
       end if
       if(xdest >= xsrc((ind+ir)/2)) then
          ind = (ind+ir)/2
       end if
    end do
    weight = (xsrc(ir)-xdest)/(xsrc(ir)-xsrc(ind))
  end subroutine find_weights_nonuniform_single_r4

  subroutine find_weights_nonuniform_single_r8(xsrc, nx, xdest, ind, weight, status)
    ! Find the weight for linear interpolation
    ! NOTE: xsrc(1:nx) contains the coordinates of the pixel centers
    ! If y(1:nx) are the source vector values, then
    ! ydest = y(ind)*weight + y(ind+1)*(1-weight)
    implicit none
    integer, parameter :: rp=kind(1d0)
    integer,  intent(in)  :: nx          ! Number of vector elements
    real(rp), intent(in)  :: xsrc(nx)    ! Coordinates of the cell centers
    real(rp), intent(in)  :: xdest
    integer,  intent(out) :: ind
    real(rp), intent(out) :: weight
    integer,  intent(out) :: status      ! Exit status code
    integer  :: ir

    status = 0     ! Default: OK

    ! Extrapolate out of boundaries
    if(xdest < xsrc(1)) then
       ind = 1
       weight = 1.0_rp     ! Keep first value
       return
    end if
    if(xdest > xsrc(nx)) then
       ind = nx-1
       weight = 0.0_rp     ! Keep last value
       return
    end if

    ! Check arguments
    if(nx <= 1) then
       status = status + 1            ! Invalid nxsrc
    endif

    ! Return on error
    if(status /= 0) return

    ! Divide and conquer method
    ir=nx
    ind=1
    do while(ind+1 /= ir)
       if(xdest < xsrc((ind+ir)/2)) then
          ir = (ind+ir)/2
       end if
       if(xdest >= xsrc((ind+ir)/2)) then
          ind = (ind+ir)/2
       end if
    end do
    weight = (xsrc(ir)-xdest)/(xsrc(ir)-xsrc(ind))
  end subroutine find_weights_nonuniform_single_r8

  subroutine find_weights_nonuniform_many_r4(xsrc, nxsrc, xdest, nxdest, ind, weight, status)
    implicit none
    integer, parameter    :: rp=kind(1.0)
    integer,  intent(in)  :: nxsrc, nxdest
    real(rp), intent(in)  :: xsrc(nxsrc)
    real(rp), intent(in)  :: xdest(nxdest)
    integer,  intent(out) :: ind(nxdest)
    real(rp), intent(out) :: weight(nxdest)
    integer,  intent(out) :: status         ! Exit status
    ! Local variables
    integer  :: i
    !
    status = 0   ! Default: OK
    !
    ! Check arguments
    if(nxsrc <= 1 .or. nxdest <= 0) then
       status = status + 1            ! Invalid nxsrc or nxdest
       return
    endif

    do i=1,nxdest
       call find_weights_nonuniform(xsrc, nxsrc, xdest(i), ind(i), weight(i), status)
       if(status /= 0) return
    end do
  end subroutine find_weights_nonuniform_many_r4

  subroutine find_weights_nonuniform_many_r8(xsrc, nxsrc, xdest, nxdest, &
       ind, weight, status)
    implicit none
    integer, parameter    :: rp=kind(1d0)
    integer,  intent(in)  :: nxsrc, nxdest
    real(rp), intent(in)  :: xsrc(nxsrc)
    real(rp), intent(in)  :: xdest(nxdest)
    integer,  intent(out) :: ind(nxdest)
    real(rp), intent(out) :: weight(nxdest)
    integer,  intent(out) :: status         ! Exit status
    ! Local variables
    integer  :: i
    !
    status = 0   ! Default: OK
    !
    ! Check arguments
    if(nxsrc <= 1 .or. nxdest <= 0) then
       status = status + 1            ! Invalid nxsrc or nxdest
       return
    endif

    do i=1,nxdest
       call find_weights_nonuniform(xsrc, nxsrc, xdest(i), ind(i), weight(i), status)
       if(status /= 0) return
    end do
  end subroutine find_weights_nonuniform_many_r8

  !
  ! Interpolation 1D
  !
  subroutine interpolate_1d_single_r4(nysrc,ysrc,ydest,ind,weight)
    implicit none
    integer,  parameter   :: rp=kind(1.0)
    integer,  intent(in)  :: nysrc
    real(rp), intent(in)  :: ysrc(nysrc)
    real(rp), intent(out) :: ydest
    integer,  intent(in)  :: ind
    real(rp), intent(in)  :: weight
    ydest=weight*ysrc(ind)+(1.0_rp-weight)*ysrc(ind+1)
  end subroutine interpolate_1d_single_r4

  subroutine interpolate_1d_single_r8(nysrc,ysrc,ydest,ind,weight)
    implicit none
    integer,  parameter   :: rp=kind(1d0)
    integer,  intent(in)  :: nysrc
    real(rp), intent(in)  :: ysrc(nysrc)
    real(rp), intent(out) :: ydest
    integer,  intent(in)  :: ind
    real(rp), intent(in)  :: weight
    ydest=weight*ysrc(ind)+(1.0_rp-weight)*ysrc(ind+1)
  end subroutine interpolate_1d_single_r8

  subroutine interpolate_1d_many_r4(nxsrc,matsrc,nxdest,matdest,ind,weight)
    implicit none
    integer,  parameter   :: rp=kind(1.0)
    integer,  intent(in)  :: nxsrc,nxdest
    real(rp), intent(in)  :: matsrc(nxsrc)
    real(rp), intent(out) :: matdest(nxdest)
    integer,  intent(in)  :: ind(nxdest)
    real(rp), intent(in)  :: weight(nxdest)
    integer :: i
    !
    do i=1,nxdest
       matdest(i)=weight(i)*matsrc(ind(i))+(1.0_rp-weight(i))*matsrc(ind(i)+1)
    end do
  end subroutine interpolate_1d_many_r4

  subroutine interpolate_1d_many_r8(nxsrc,matsrc,nxdest,matdest,ind,weight)
    implicit none
    integer,  parameter   :: rp=kind(1d0)
    integer,  intent(in)  :: nxsrc,nxdest
    real(rp), intent(in)  :: matsrc(nxsrc)
    real(rp), intent(out) :: matdest(nxdest)
    integer,  intent(in)  :: ind(nxdest)
    real(rp), intent(in)  :: weight(nxdest)
    integer :: i
    !
    do i=1,nxdest
       matdest(i)=weight(i)*matsrc(ind(i))+(1.0_rp-weight(i))*matsrc(ind(i)+1)
    end do
  end subroutine interpolate_1d_many_r8

  !
  ! Interpolation 2D
  !
  subroutine interpolate_2d_single_r4(nxsrc,nysrc,matsrc, &
       dest,xindex,xweight,yindex,yweight)
    implicit none
    integer, parameter  :: rp=kind(1.0)
    integer, intent(in) :: nxsrc,nysrc
    real(rp), intent(in)    :: matsrc(nxsrc,nysrc)
    real(rp), intent(out)   :: dest
    integer, intent(in) :: xindex,yindex
    real(rp), intent(in)    :: xweight,yweight
    real(rp)    :: vlow,vhigh
    !
    vlow=xweight*matsrc(xindex,yindex) + &
         (1.0_rp-xweight)*matsrc(xindex+1,yindex)
    vhigh=xweight*matsrc(xindex,yindex+1) + &
         (1.0_rp-xweight)*matsrc(xindex+1,yindex+1)
    dest = vlow*yweight+vhigh*(1.0_rp-yweight)
  end subroutine interpolate_2d_single_r4

  subroutine interpolate_2d_single_r8(nxsrc,nysrc,matsrc, &
       dest,xindex,xweight,yindex,yweight)
    implicit none
    integer, parameter  :: rp=kind(1d0)
    integer, intent(in) :: nxsrc,nysrc
    real(rp), intent(in)    :: matsrc(nxsrc,nysrc)
    real(rp), intent(out)   :: dest
    integer, intent(in) :: xindex,yindex
    real(rp), intent(in)    :: xweight,yweight
    real(rp)    :: vlow,vhigh
    !
    vlow=xweight*matsrc(xindex,yindex) + &
         (1.0_rp-xweight)*matsrc(xindex+1,yindex)
    vhigh=xweight*matsrc(xindex,yindex+1) + &
         (1.0_rp-xweight)*matsrc(xindex+1,yindex+1)
    dest = vlow*yweight+vhigh*(1.0_rp-yweight)
  end subroutine interpolate_2d_single_r8

  subroutine interpolate_2d_many_r4(nxsrc,nysrc,matsrc,nxdest,nydest, &
       matdest,xindex,xweight,yindex,yweight)
    implicit none
    integer, parameter   :: rp=kind(1.0) ! Set the real precision
    integer, intent(in)  :: nxsrc,nysrc,nxdest,nydest
    real(rp), intent(in) :: matsrc(nxsrc,nysrc)
    real(rp), intent(out):: matdest(nxdest,nydest)
    integer, intent(in)  :: xindex(nxdest),yindex(nydest)
    real(rp), intent(in) :: xweight(nxdest),yweight(nydest)
    integer  :: i,j
    real(rp) :: vlow,vhigh
    !
    do i=1,nxdest
       do j=1,nydest
          vlow=xweight(i)*matsrc(xindex(i),yindex(j)) + &
               (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j))
          vhigh=xweight(i)*matsrc(xindex(i),yindex(j)+1) + &
               (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1)
          matdest(i,j) = vlow*yweight(j)+vhigh*(1.0_rp-yweight(j))
       end do
    end do
  end subroutine interpolate_2d_many_r4

  subroutine interpolate_2d_many_r8(nxsrc,nysrc,matsrc,nxdest,nydest, &
       matdest,xindex,xweight,yindex,yweight)
    implicit none
    integer, parameter   :: rp=kind(1d0) ! Set the real precision
    integer, intent(in)  :: nxsrc,nysrc,nxdest,nydest
    real(rp), intent(in) :: matsrc(nxsrc,nysrc)
    real(rp), intent(out):: matdest(nxdest,nydest)
    integer, intent(in)  :: xindex(nxdest),yindex(nydest)
    real(rp), intent(in) :: xweight(nxdest),yweight(nydest)
    integer  :: i,j
    real(rp) :: vlow,vhigh
    !
    do i=1,nxdest
       do j=1,nydest
          vlow=xweight(i)*matsrc(xindex(i),yindex(j)) + &
               (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j))
          vhigh=xweight(i)*matsrc(xindex(i),yindex(j)+1) + &
               (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1)
          matdest(i,j) = vlow*yweight(j)+vhigh*(1.0_rp-yweight(j))
       end do
    end do
  end subroutine interpolate_2d_many_r8
  !
  ! Interpolation 3D
  !
  subroutine interpolate_3d_many_r4(nxsrc,nysrc,nzsrc,matsrc,nxdest,nydest, &
       nzdest,matdest,xindex,xweight,yindex,yweight,zindex,zweight)
    implicit none
    integer, parameter   :: rp=kind(1.0)  ! Set the real precision
    integer, intent(in)  :: nxsrc,nysrc,nzsrc,nxdest,nydest,nzdest
    real(rp), intent(in) :: matsrc(nxsrc,nysrc,nzsrc)
    real(rp), intent(out):: matdest(nxdest,nydest,nzdest)
    integer, intent(in)  :: xindex(nxdest),yindex(nydest),zindex(nzdest)
    real(rp), intent(in) :: xweight(nxdest),yweight(nydest),zweight(nzdest)
    integer  :: i,j,k
    real(rp) :: vb,vbl,vbh,vu,vul,vuh
    !
    do i=1,nxdest
       do j=1,nydest
          do k=1,nzdest
             vbl=xweight(i)*matsrc(xindex(i),yindex(j),zindex(k)) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j),zindex(k))
             vbh=xweight(i)*matsrc(xindex(i),yindex(j)+1,zindex(k)) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1,zindex(k))
             vb = vbl*yweight(j)+vbh*(1.0_rp-yweight(j))
             vul=xweight(i)*matsrc(xindex(i),yindex(j),zindex(k)+1) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j),zindex(k)+1)
             vuh=xweight(i)*matsrc(xindex(i),yindex(j)+1,zindex(k)+1) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1,zindex(k)+1)
             vu = vul*yweight(j)+vuh*(1.0_rp-yweight(j))
             matdest(i,j,k) = vb*zweight(k)+vu*(1.0_rp-zweight(k))
          end do
       end do
    end do
  end subroutine interpolate_3d_many_r4

  subroutine interpolate_3d_many_r8(nxsrc,nysrc,nzsrc,matsrc,nxdest,nydest, &
       nzdest,matdest,xindex,xweight,yindex,yweight,zindex,zweight)
    implicit none
    integer, parameter   :: rp=kind(1d0)  ! Set the real precision
    integer, intent(in)  :: nxsrc,nysrc,nzsrc,nxdest,nydest,nzdest
    real(rp), intent(in) :: matsrc(nxsrc,nysrc,nzsrc)
    real(rp), intent(out):: matdest(nxdest,nydest,nzdest)
    integer, intent(in)  :: xindex(nxdest),yindex(nydest),zindex(nzdest)
    real(rp), intent(in) :: xweight(nxdest),yweight(nydest),zweight(nzdest)
    integer  :: i,j,k
    real(rp) :: vb,vbl,vbh,vu,vul,vuh
    !
    do i=1,nxdest
       do j=1,nydest
          do k=1,nzdest
             vbl=xweight(i)*matsrc(xindex(i),yindex(j),zindex(k)) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j),zindex(k))
             vbh=xweight(i)*matsrc(xindex(i),yindex(j)+1,zindex(k)) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1,zindex(k))
             vb = vbl*yweight(j)+vbh*(1.0_rp-yweight(j))
             vul=xweight(i)*matsrc(xindex(i),yindex(j),zindex(k)+1) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j),zindex(k)+1)
             vuh=xweight(i)*matsrc(xindex(i),yindex(j)+1,zindex(k)+1) + &
                  (1.0_rp-xweight(i))*matsrc(xindex(i)+1,yindex(j)+1,zindex(k)+1)
             vu = vul*yweight(j)+vuh*(1.0_rp-yweight(j))
             matdest(i,j,k) = vb*zweight(k)+vu*(1.0_rp-zweight(k))
          end do
       end do
    end do
  end subroutine interpolate_3d_many_r8

end module m_interp
