subroutine diffx(c,nx,ny,nz,dx,dt,rkhor)
  ! Diffusion (X)
  use kindtype
  implicit none
  !
  integer, intent(in) :: nx,ny,nz
  real(rp), intent(in) :: dx,dt
  real(rp), intent(in) :: rkhor(nx,ny,nz)
  real(rp), intent(inout) :: c(0:nx+1,0:ny+1,0:nz+1)
  !
  real(rp), parameter :: D8MIN=1e-13_rp
  real(rp) :: dtdx2,rkx1,rkx2,rks1,rks2
  integer :: i,j,k,im1,ip1
  logical :: arithmetic_mean = .true.
  !
  dtdx2=dt/dx**2
  !
  do k=1,nz
     do j=1,ny
        do i=1,nx
           im1=max(1,i-1)
           ip1=min(i+1,nx)
           !
           rks1=0.5_rp*(rkhor(im1,j,k)+rkhor(i,j,k))
           rks2=0.5_rp*(rkhor(i,j,k)+rkhor(ip1,j,k))
           !
           if(arithmetic_mean) then
              ! Aritmetic mean
              rkx1=rks1
              rkx2=rks2
           else
              ! Geometric mean
              if(rks1 > D8MIN) then
                 rkx1=(rkhor(im1,j,k)*rkhor(i,j,k))/rks1
              else
                 rkx1=0.0_rp
              endif
              if(rks2 > D8MIN) then
                 rkx2=(rkhor(i,j,k)*rkhor(ip1,j,k))/rks2
              else
                 rkx2=0.0_rp
              endif
           endif
           !
           c(i,j,k) = c(i,j,k) + dtdx2*(rkx1*c(i-1,j,k) - &
                (rkx1+rkx2)*c(i,j,k)+rkx2*c(i+1,j,k))
        enddo
     enddo
  enddo
  return
end subroutine diffx
