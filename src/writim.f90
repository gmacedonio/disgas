subroutine writim
  !***************************************************************************
  !*
  !*    Prints time evolution to the log file
  !*
  !**************************************************************************
  use inpout
  use master
  implicit none
  !
  integer :: iyr,imo,idy,ihr,imi,ise
  !
  !***  Initializations
  !
  !
  !***  Writes to the log file
  !
  call addtime(ibyr,ibmo,ibdy,ibhr,ibmi,iyr,imo,idy,ihr,imi,ise,time)
  !
  write(nlog,10) icounter,dt,time-tbeg,        &
       ((time-tbeg)/trun)*100.0_rp,              &
       idy ,month(imo ),iyr ,ihr ,imi ,ise
10 format(/,                                                       &
       '   Iteration                 : ',i8                   ,/,  &
       '   Time step                 : ',e13.6,' s'           ,/,  &
       '   Elapsed time              : ',f10.2,' s   (',f7.2,'%)',/,  &
       '   Current time              : ',i2,1x,a3,1x,i4,' at ', &
       i2.2,':',i2.2,':',i2.2 ,/, &
       '   -------                     ')
  !
  flush(nlog)
  !
  !***  Writes tracking points
  !
  !  if(out_pts) then
  !     call wripts
  !  end if
  !
  !***  Writes tracking boxes
  !
  !  if(out_box) then
  !     call wribox
  !  end if
  !
  return
end subroutine writim
