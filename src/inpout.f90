MODULE inpout
  !***************************************************************
  !*
  !*		Module for input/output
  !*
  !***************************************************************
  use kindtype
  implicit none
  save
  !
  !***  File logical units
  !
  integer :: nlog = 1      ! Default log file. May be also 6 for terminal.
  integer :: ndia = 8      ! Meteo file unit (DWM)
  !
  integer :: grd_type      ! Output GRD type (0=ASCII, 1=BINARY)
  !
  !***  File names
  !
  character(len=250) :: flog,finp,ftop,frou,fmet,fdia  ! File names
  character(len=250) :: frst,fsrc,fpts,fbox            ! File names
  character(len=250) :: outdir                         ! Output directory
  !
  ! Sequential number for output files
  integer :: fseqnum=0  ! It is zero or set to last value in case of restart
  !
  !***  Postprocess variables
  !
  logical :: out_rst  ! Write the restart file
  logical :: out_dom,out_src,out_u,out_v,out_w,out_c,out_g
  real(rp) :: tevery
  !
  !***  Tracking points
  !
  ! logical :: out_pts,out_box   ! Output track points/boxes
  ! integer :: npts
  ! integer, allocatable :: i_pts(:)     ! mesh index for track points
  ! integer, allocatable :: j_pts(:)     ! mesh index for track points
  !
  ! real(rp), allocatable :: x_pts(:)    ! x(npts) x-coord. for track points
  ! real(rp), allocatable :: y_pts(:)    ! y(npts) y-coord. for track points
  ! real(rp), allocatable :: z_pts(:)    ! z(npts) z-coord. for track points
  ! real(rp), allocatable :: s_pts(:,:)  ! interpolation shape function for tracking points
  !
  !***  Tracking boxes
  !
  ! integer :: nbox
  ! integer, allocatable :: i1_box(:)     ! mesh index for box
  ! integer, allocatable :: j1_box(:)     ! mesh index for box
  ! integer, allocatable :: i2_box(:)     ! mesh index for box
  ! integer, allocatable :: j2_box(:)     ! mesh index for box
  !
  ! real(rp), allocatable :: x_box(:)     ! x (nbox) x-coord. for track boxes
  ! real(rp), allocatable :: y_box(:)     ! y (nbox) y-coord. for track boxes
  ! real(rp), allocatable :: z_box(:)     ! z (nbox) z-coord. for track boxes
  !
CONTAINS
  !
  !
  subroutine get_input_npar(fname,myblk,line,npar,istat,message)
    !**************************************************************************
    !*
    !*    Gets the number of parameters associated to a line
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*
    !*    OUTPUT:
    !*    integer*4       npar     Number of parameters
    !*    integer*4       istat    0 if OK
    !*    character*(*)   message  Output message with the error description
    !*
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    integer            :: npar,istat
    !
    logical            :: linefound,blockfound
    integer            :: nword
    character(len=256) :: card
    character(len=256) :: words(128)
    real     (rp)      :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==   &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = ' '
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'Block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'Line '//TRIM(line)//' not found in the input file'
    close(90)
    return
  end subroutine get_input_npar

  subroutine get_input_int4(fname,myblk,line,myval,nval,istat,message)
    !**************************************************************************
    !*
    !*    Gets nval integer*4 inputs from the file fname
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*    integer*4       nval     Number of integers to read
    !*
    !*    OUTPUT:
    !*    integer*4       istat    0 if OK
    !*    integer*4       myval    Values of the nval integers read
    !*    character*100   message  Output message with the error description
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    integer    :: nval,istat
    integer    :: myval
    !
    logical            :: linefound,blockfound
    integer    :: nword,npar
    character(len=256) :: card
    character(len=256) :: words(128)
    real     (rp) :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==       &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    if(npar < nval) goto 104
    !
    !      do ival = 1,nval
    !	     myval(ival) = INT(param(ival))
    !      end do
    myval = INT(param(1))
    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = ' '
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'get_input_int4: block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'get_input_int4: line '//TRIM(line)//' not found in the input file'
    close(90)
    return
104 message = 'get_input_int4: too few parameters in line '//TRIM(line)
    close(90)
    return
  end subroutine get_input_int4
  !
  !
  subroutine get_input_rea8(fname,myblk,line,myval,nval,istat,message)
    !**************************************************************************
    !*
    !*    Gets nval real*8 inputs from the file fname
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*    integer*4       nval     Number of integers to read
    !*
    !*    OUTPUT:
    !*    integer*4       istat    0 if OK
    !*    real*8          myval    Values of the nval integers read
    !*    character*100   message  Output message with the error description
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    integer            :: nval,istat
    real   (rp)        :: myval
    !
    logical            :: linefound,blockfound
    integer            :: nword,npar
    character(len=256) :: card
    character(len=256) :: words(128)
    real     (rp)      :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==   &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    if(nval==1) myval = param(1)

    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = 'OK'
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'Block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'Line '//TRIM(line)//' not found in the input file'
    close(90)
    return
  end subroutine get_input_rea8
  !
  !
  subroutine get_input_ivec(fname,myblk,line,myval,nval,istat,message)
    !**************************************************************************
    !*
    !*    Gets nval integer inputs from the file fname
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*    integer*4       nval     Number of integers to read
    !*
    !*    OUTPUT:
    !*    integer*4       istat    0 if OK
    !*    integer*4       myval    Values of the nval integers read
    !*    character*100   message  Output message with the error description
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    integer            :: nval,istat
    integer            :: myval(nval)
    !
    logical            :: linefound,blockfound
    integer            :: nword,npar,ival
    character(len=256) :: card
    character(len=256) :: words(128)
    real(rp)           :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==   &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    !    if(npar < nval) goto 104
    !
    do ival = 1,nval
       myval(ival) = nint(param(ival))
    end do
    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = 'OK'
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'Block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'Line '//TRIM(line)//' not found in the input file'
    close(90)
    return
  end subroutine get_input_ivec
  !
  !
  !
  subroutine get_input_vec8(fname,myblk,line,myval,nval,istat,message)
    !**************************************************************************
    !*
    !*    Gets nval real*8 inputs from the file fname
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*    integer*4       nval     Number of integers to read
    !*
    !*    OUTPUT:
    !*    integer*4       istat    0 if OK
    !*    real*8          myval    Values of the nval integers read
    !*    character*100   message  Output message with the error description
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    integer            :: nval,istat
    real   (rp)        :: myval(nval)
    !
    logical            :: linefound,blockfound
    integer            :: nword,npar,ival
    character(len=256) :: card
    character(len=256) :: words(128)
    real     (rp)      :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==   &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    !    if(npar < nval) goto 104
    !
    do ival = 1,nval
       myval(ival) = param(ival)
    end do
    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = 'OK'
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'Block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'Line '//TRIM(line)//' not found in the input file'
    close(90)
    return
  end subroutine get_input_vec8
  !
  !
  !
  subroutine get_input_cha(fname,myblk,line,myval,nval,istat,message)
    !**************************************************************************
    !*
    !*    Gets nval character inputs from the file fname
    !*
    !*    NOTE: words are converted to UPPER case
    !*
    !*    INPUT:
    !*    character*(*)   fname    Name of the file
    !*    character*(*)   myblk    Block to search
    !*    character*(*)   line     Line block to search
    !*    integer*4       nval     Number of integers to read
    !*
    !*    OUTPUT:
    !*    integer*4       istat    0 if OK
    !*    character*(*)   myval    Values of the nval integers read
    !*    character*100   message  Output message with the error description
    !*
    !**************************************************************************
    use kindtype
    implicit none
    !
    integer            :: nval,istat
    character(len=100) :: message
    character*(*)      :: fname,myblk,line
    character*(*)      :: myval
    !
    logical            :: linefound,blockfound
    integer            :: nword,npar,j
    character(len=256) :: card
    character(len=256) :: words(128)
    real(rp)           :: param(128)
    !
    !***  Initializations
    !
    istat = -1
    !
    !***  Opens the file
    !
    open(90,FILE=TRIM(fname),STATUS='old',ERR=101)
    !
    !***  Search the line
    !
    blockfound = .false.
    linefound  = .false.
    do while(.not.linefound)
       do while(.not.blockfound)
          read(90,'(a256)',END=102) card
          call decode(card,words,param,nword,npar)
          if(words(1)(1:LEN_TRIM(myblk))==   &
               myblk(1:LEN_TRIM(myblk))) blockfound=.true.
       end do
       read(90,'(a256)',END=103) card
       call decode(card,words,param,nword,npar)
       if(words(1)(1:LEN_TRIM(line))==   &
            line(1:LEN_TRIM(line))) linefound = .true.
    end do
    !
    if((nword-1) < nval) goto 104
    !
    myval(1:LEN_TRIM(words(2))) = words(2)(1:LEN_TRIM(words(2)))
    !
    !***     Fill the rest with ' '
    !
    do j=LEN_TRIM(words(2))+1,LEN(myval)
       myval(j:j)=' '
    end do
    !
    !***  Successful end
    !
    close(90)
    istat = 0
    message = 'OK'
    return
    !
    !***  List of errors
    !
101 message = 'Error opening the input file: '//TRIM(fname)
    return
102 message = 'get_input_cha: block '//TRIM(myblk)//' not found in the input file'
    close(90)
    return
103 message = 'get_input_cha: line '//TRIM(line)//' not found in the input file'
    close(90)
    return
104 message = 'get_input_cha: too few parameters in line '//TRIM(line)
    close(90)
    return
  end subroutine get_input_cha
  !
  subroutine decode(card,words,param,nword,npar)
    !********************************************************************
    !*
    !*  This routine decodes a string card(256) into words and parameters
    !*
    !********************************************************************
    use kindtype
    implicit none
    integer            :: nword,npar
    character(len=256) :: card
    character(len=256) :: words(128)
    character(len=256) :: s
    real(rp)           :: param(128)
    !
    integer    :: ipos,first,last,nstr,lflag,i
    real(rp)   :: digit
    !
    !***  Initializations
    !
    nword = 0
    npar  = 0
    ipos  = 0
    !
    do while( .true. )
       !                                    ! First position
       ipos = ipos + 1
       if(ipos > 256) return
10     if(card(ipos:ipos)==' '.or.card(ipos:ipos)=='=') then
          ipos = ipos + 1
          if(ipos > 256) return
          go to 10
       end if
       first = ipos
       !
       ipos = ipos + 1
       if(ipos > 256) return
20     if(card(ipos:ipos) /= ' '.and.card(ipos:ipos) /= '=') then
          ipos = ipos + 1
          if(ipos > 256) return
          go to 20
       end if
       last = ipos-1
       !
       nstr = last-first+1
       s(1:nstr) = card(first:last)
       call decod1(s,nstr,lflag,digit)
       if(lflag==0) then
          npar = npar + 1
          param(npar)= digit
       else if(lflag==1) then
          nword = nword + 1
          do i=1,256
             words(nword)(i:i) = ' '
          end do
          words(nword)(1:nstr) = card(first:last)
       end if
       !
    end do
  end subroutine decode
  !
  subroutine decod1(string,nstr,lflag,digit)
    !*******************************************************************
    !*
    !*    This subroutine decodes a single string(1:nstr)
    !*
    !*    If string(1:nstr) is a string returns lflag = 1
    !*    If string(1:nstr) is a number returns lflag = 0 and the digit
    !*
    !*******************************************************************
    use kindtype
    implicit none
    integer     :: nstr,lflag
    integer     :: istr,decim
    real(rp)    :: digit
    character(len=1):: string(nstr)
    !
    lflag = 0                                             ! Number by default
    istr  = 1
    !
    do while(istr <= nstr)
       decim = ichar(string(istr))                       ! decimal value
       if(decim < 48.or.decim > 57) then                 ! It is not a num.
          if(decim /= 43 .and.decim /= 45 .and. &        ! discard + -
               decim /= 68 .and.decim /= 69 .and. &      ! discard D E
               decim /= 100.and.decim /= 101.and. &      ! discard d e
               decim /= 46) then                         ! discard .
             lflag = 1
             istr  = nstr
          end if
       end if
       istr = istr+1
    end do
    !
    if(lflag==0) then                                   ! It's a number
       digit = stof(string,nstr)
    end if
    !
    return
  end subroutine decod1
  !
  function stof(string,nstr)
    !**************************************************************
    !*
    !*    This routine converts a real/integer number stored in a
    !*    string(1:nstr) into a real(rp) digit format
    !*
    !**************************************************************
    use kindtype
    implicit none
    integer         ::  nstr
    real(rp) :: stof
    character(len=1)::  string(*)
    !
    integer      :: i,ipos,nsign,esign,nvalu
    integer      :: expo,valu(256)
    logical next
    !
    stof = 0.0
    !
    !***  Sing decoding
    !
    ipos = 1
    if(ichar(string(ipos))==43) then         !  + sign
       nsign = 1
       ipos  = ipos + 1
    else if(ichar(string(ipos))==45) then    !  - sign
       nsign = -1
       ipos  = ipos + 1
    else                                       !  no sing (+)
       nsign = 1
       ipos  = ipos
    end if
    !
    !***  Base decoding
    !
    nvalu = 0
    next  = .true.
    do while(next)
       if((ichar(string(ipos))==68 ).or.    &      ! D
            (ichar(string(ipos))==69 ).or.  &      ! E
            (ichar(string(ipos))==100).or.  &      ! d
            (ichar(string(ipos))==101).or.  &      ! e
            (ichar(string(ipos))==46 )) then       ! .
          next = .false.
       else
          nvalu = nvalu + 1
          valu(nvalu) = stof1(string(ipos))
          ipos = ipos + 1
          if(ipos==(nstr+1)) then
             next = .false.
             ipos = ipos - 1
          end if
       end if
    end do
    do i = 1,nvalu
       stof = stof + valu(i)*10.0_rp**(nvalu-i)
    end do
    !
    !***  Decimal decoding
    !
    if((ichar(string(ipos))==46   ).and. &
         ipos /= nstr) then
       ipos = ipos + 1
       nvalu = 0
       next  = .true.
       do while(next)
          if((ichar(string(ipos))==68 ).or.     &      ! D
               (ichar(string(ipos))==69 ).or.   &      ! E
               (ichar(string(ipos))==100).or.   &      ! d
               (ichar(string(ipos))==101)) then        ! e
             next = .false.
          else
             nvalu = nvalu + 1
             valu(nvalu) = stof1(string(ipos))
             ipos = ipos + 1
             if(ipos==(nstr+1)) then
                next = .false.
                ipos = ipos - 1
             end if
          end if
       end do
       do i = 1,nvalu
          stof = stof + valu(i)*10.0_rp**(-i)
       end do
    end if
    !
    !***  Exponent
    !
    if(((ichar(string(ipos))==68 ).or.    &      ! D
         (ichar(string(ipos))==69 ).or.   &      ! E
         (ichar(string(ipos))==100).or.   &      ! d
         (ichar(string(ipos))==101)).and. &      ! e
         ipos /= nstr) then
       ipos = ipos + 1
       if(ichar(string(ipos))==43) then         !  + sign
          esign = 1
          ipos  = ipos + 1
       else if(ichar(string(ipos))==45) then    !  - sign
          esign = -1
          ipos  = ipos + 1
       else                                       !  no sing (+)
          esign = 1
          ipos  = ipos
       end if
       !
       nvalu = 0
       next  = .true.
       do while(next)
          nvalu = nvalu + 1
          valu(nvalu) = stof1(string(ipos))
          ipos = ipos + 1
          if(ipos==(nstr+1)) then
             next = .false.
             ipos = ipos - 1
          end if
       end do
       expo = 0
       do i = 1,nvalu
          expo = expo + valu(i)*10**(nvalu-i)
       end do
       expo = esign*expo
       !
       stof = stof*(10.0_rp**expo)
    end if
    !
    stof = nsign*stof
  end function stof
  !
  integer function stof1(string1)
    !**************************************************************
    !*
    !*    Decodes a character*1 string
    !*
    !**************************************************************
    implicit none
    character(len=1) :: string1
    !
    if(string1=='0') then
       stof1 = 0
    else if(string1=='1') then
       stof1 = 1
    else if(string1=='2') then
       stof1 = 2
    else if(string1=='3') then
       stof1 = 3
    else if(string1=='4') then
       stof1 = 4
    else if(string1=='5') then
       stof1 = 5
    else if(string1=='6') then
       stof1 = 6
    else if(string1=='7') then
       stof1 = 7
    else if(string1=='8') then
       stof1 = 8
    else if(string1=='9') then
       stof1 = 9
    else
       stof1 = 0   ! Default (should not happen)
    end if
    return
  end function stof1

end module inpout
