subroutine setustarmol
  !****************************************************************************
  !*
  !*     This routine evaluates local values of ustar and Monin-Obukov Length
  !*     by calling subroutine abl for each grid cell
  !*
  !*     INPUTS:
  !*      rlength(i,j)  - Local roughness length
  !*      Zref          - Reference level
  !*      Tz0           - Ground temberature (K)
  !*      Tzref         - Temperature at Zref (K)
  !*      pres          - Pressure (Pa) (provided if need_pres=.false.)
  !*      u(i,j,k)      - u-velocity
  !*      v(i,j,k)      - v-velocity
  !*      need_pres     - logical (set by readuni or readdwm)
  !*
  !*     OUTPUTS:
  !*      ustar(i,j)    - Friction velocity
  !*      rmonin(i,j)   - Monin-Obukhov length
  !*
  !****************************************************************************
  use kindtype
  use master, only: nx,ny,nz,vx,vy,zlayer,zref,tz0,Tzref,pres
  use master, only: topg,rlength,ustar,rmonin
  use master, only: need_pres
  implicit none
  real(rp) :: u,v,wstar
  real(rp) :: plocal      ! Local atmospheric pressure
  real(rp) :: sigma,delta,theta
  real(rp) :: w1,w2       ! Weigths for velocity interpolation
  integer  :: ind         ! Index of upper node for velocity interpolation
  !
  ! Set pressure at sea level (US Standard Atmosphere 1977)
  real(rp), parameter :: pres0 = 101325.0_rp ! Ref. pressure at sea level
  !
  integer :: i,j
  !
  ! Find weights for interpolation
  ! Search ind such that zlayer(ind-1) <= zref < zlayer(ind)
  ind=2
  do i=2,nz
     if(zref <= zlayer(i)) exit  ! Zref is always <= maxval(zlayer)
     ind = i
  enddo
  w1 = (zlayer(ind)-zref)/(zlayer(ind)-zlayer(ind-1))
  w2 = (zref-zlayer(ind-1))/(zlayer(ind)-zlayer(ind-1))
  !
  do i=1,nx
     do j=1,ny
        !
        ! Atmospheric pressure
        if(.not.need_pres) then
           plocal = pres
        else
           call atmosphere(topg(i,j),sigma,delta,theta) ! Standard atmosphere
           plocal = delta*pres0
        endif
        ! Interpolate u,v at zref
        u = vx(i,j,ind-1)*w1 + vx(i,j,ind)*w2
        v = vy(i,j,ind-1)*w1 + vy(i,j,ind)*w2
        !
        call abl(rlength(i,j),zref,Tz0,Tzref,plocal,u,v,  &
             ustar(i,j),wstar,rmonin(i,j))
        !
     enddo
  enddo
  !
end subroutine setustarmol
