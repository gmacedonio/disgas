subroutine setsrc
  !****************************************************************
  !*
  !*   Reads the source file
  !*
  !*      FORMAT:
  !*         x y z fluxm
  !*      where flux is the mass flow rate (kg/s)
  !*
  !****************************************************************
  use kindtype
  use inpout, only: fsrc, nlog, grd_type, out_src, outdir
  use master, only: x0, y0, flux, nx, ny, nz, dx, dy, zlayer
  use rwgrd
  implicit none
  !
  character(len=250) :: fname
  integer  :: ix,iy,iz,i
  real(rp) :: xs,ys,zs,fluxm
  real(rp) :: dist,distmin
  real(8)  :: xmax, ymax
  integer  :: status
  integer  :: ntsrc      ! Total number of sources in the input file
  integer  :: nisrc      ! Number of sources inside the domain
  character(len=255) :: record, logmsg
  !
  !*** Opens the file
  !
  open(90,FILE=TRIM(fsrc),STATUS='old',ERR=100)
  !
  ! Reads the discrete values
  !
  ntsrc = 0
  nisrc = 0
  do
     read(90, '(a)', end=1, err=101) record
     ntsrc = ntsrc + 1
     read(record,*,end=101,err=101) xs,ys,zs,fluxm
     !
     ! Find cell
     ix=int((xs-x0)/dx)+1
     iy=int((ys-y0)/dy)+1
     iz = 1
     ! Find nearest layer
     distmin = huge(1.0_rp)
     do i=1,nz
        dist = abs(zlayer(i)-zs)
        if(dist <= distmin) then
           iz = i
           distmin = dist
        endif
     enddo
     !
     ! Check if the source is strictly inside the domain
     if(ix < 1 .or. ix > nx .or. iy < 1 .or. iy > ny .or. &
          zs < zlayer(1) .or. zs > zlayer(nz)) then
        write(nlog,*) 'Source point outside domain (rejected):',xs,ys,zs
     else
        flux(ix,iy,iz) = flux(ix,iy,iz) + fluxm
        nisrc = nisrc + 1
     endif
  enddo
  !
1 continue
  close(90)
  !
  !*** If necessary, outputs the source term
  !
  xmax = x0 + nx*dx
  ymax = y0 + ny*dy
  if(out_src) then
     call runend(-1,'SETSRC: DUMP SOURCE NOT YET IMPLEMENTED')
     ! @@@@
     fname = TRIM(outdir)//'/'//'source.grd'
     call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,flux,status)
     if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
  end if

  ! Write in the log file
  write(nlog, '(/,''------------'')')
  write(nlog, '('' SOURCES'')')
  write(nlog, '(''------------'')')
  write(nlog, '(''  Number of point sources in the input file: '',i8)') ntsrc
  write(nlog, '(''  Number of point sources inside the domain: '',i8)') nisrc
  !
  return
  !
  !*** List of errors
  !
100 call runend(-1,'Error opening file: '//TRIM(fsrc))
101 write(logmsg, '(''Error reading file: '',a,'' at record: '',i8)') &
         trim(fsrc), ntsrc
  call runend(-1, logmsg)
  !
  return
end subroutine setsrc
