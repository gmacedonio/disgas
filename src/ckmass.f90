subroutine ckmass(c,nx,ny,nz,dx,dy,dz,topg,ztop,map)
  ! Check total mass in the atmosphere
  !
  !     map      - Apply Z-mapping: 0=NO; 1=YES (Input)
  !
  use kindtype
  use inpout, only: nlog
  use master, only: verbose
  implicit none
  !
  integer  :: nx,ny,nz,map
  real(rp) :: dx,dy,dz(0:nz),topg(nx,ny),ztop
  real(rp) :: c(0:nx+1,0:ny+1,0:nz+1)
  integer  :: i,j,k
  real(rp) :: fatt,cmin,cmax,vol,dzeta,rjac,totm
  !
  totm = 0.0_rp
  cmax = 0.0_rp
  cmin = 1e30_rp   ! Arbitrary large number
  rjac = 1.0_rp
  !
  do k=1,nz
     dzeta=0.5_rp*(dz(k-1)+dz(k))
     !
     do j=1,ny
        do i=1,nx
           if(map == 1) rjac=(ztop-topg(i,j))/ztop
           vol = dx*dy*dzeta*rjac
           cmax=max(cmax,c(i,j,k))
           cmin=min(cmin,c(i,j,k))
           fatt=1.0_rp
           ! @@    if(i == 1.or.i == nx) fatt = 0.5_rp*fatt
           ! @@    if(j == 1.or.j == ny) fatt = 0.5_rp*fatt
           ! @@    if(k == 1.or.k == nz) fatt = 0.5_rp*fatt
           totm=totm+fatt*c(i,j,k)*vol
        enddo
     enddo
  enddo
  !
  if(verbose >= 1) then
     write(nlog,'('' Cmin,Cmax: '',2(1x,g13.5e3))') cmin,cmax
     write(nlog,'('' Mass in the atmosphere: '',g13.5e3)') totm
  endif
  !
end subroutine ckmass
