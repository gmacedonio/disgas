subroutine read_block_files
  !****************************************************************
  !*
  !*    Reads FILES BLOCK from the input file
  !*
  !****************************************************************
  use inpout
  implicit none
  !
  character(len=100) :: message
  integer            :: istat
  !
  !*** FILES block
  !
  call get_input_cha(finp,'FILES','TOPOGRAPHY_FILE_PATH',ftop,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'FILES','ROUGHNESS_FILE_PATH',frou,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'FILES','RESTART_FILE_PATH',frst,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'FILES','SOURCE_FILE_PATH',fsrc,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'FILES','WIND_FILE_PATH',fmet,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'FILES','DIAGNO_FILE_PATH',fdia,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  !  if(out_pts) then
  !     call get_input_cha(finp,'FILES','TRACK_POINTS_FILE_PATH',fpts,1,istat,message)
  !     if(istat /= 0) call runend(-1,message)
  !  else
  !     fpts = ' '
  !  end if
  !
  !  if(out_box) then
  !     call get_input_cha(finp,'FILES','BOXES_POINTS_FILE_PATH',fbox,1,istat,message)
  !     if(istat /= 0) call runend(-1,message)
  !  else
  !     fbox = ' '
  !  end if
  !
  call get_input_cha(finp,'FILES','OUTPUT_DIRECTORY',outdir,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  ! Create the output directory
  call system('mkdir -p ' // TRIM(outdir) )
  !
  return
end subroutine read_block_files
