subroutine read_block_meteo
  !****************************************************************
  !*
  !*    Reads METEO BLOCK from the input file
  !*
  !*    Date: 20-NOV-2023
  !*
  !****************************************************************
  use inpout
  use master, only: rlength0,rlength,rkh0,rkv0
  use m_kappa, only: kvmin,khmin
  use master, only: wind_model,th_model,tv_model,rough_model,strcompare
  use master, only: nx,ny,x0,y0,dx,dy
  implicit none
  !
  character(len=100) :: message,word
  integer            :: istat
  !
  !***  METEO block
  !
  call get_input_cha(finp,'METEO','WIND_MODEL',wind_model,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'METEO','HORIZONTAL_TURB_MODEL',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  if(strcompare(word,'CONSTANT')) then
     th_model = 0
  elseif(strcompare(word,'SMAGORINSKY')) then
     th_model = 1
  else
     call runend(-1,'Invalid flag HORIZONTAL_TURB_MODEL in the input file')
  end if
  !
  call get_input_cha(finp,'METEO','VERTICAL_TURB_MODEL',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  if(strcompare(word,'CONSTANT')) then
     tv_model = 0
  elseif(strcompare(word,'SIMILARITY')) then
     tv_model = 1
  else
     call runend(-1,'Invalid flag VERTICAL_TURB_MODEL in the input file')
  end if
  !
  call get_input_rea8(finp,'METEO','ROUGHNESS_LENGTH',rlength0,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha(finp,'METEO','ROUGHNESS_MODEL',rough_model,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'METEO','DIFF_COEFF_HORIZONTAL',rkh0,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'METEO','DIFF_COEFF_VERTICAL',rkv0,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  khmin = 1.0_rp  ! Default value
  call get_input_rea8(finp,'METEO','MIN_DIFF_COEFF_HORIZONTAL',khmin,1,istat,message)
  ! if(istat /= 0) call runend(-1,message)  ! Optional: no check
  !
  kvmin = 1.0_rp  ! Default value
  call get_input_rea8(finp,'METEO','MIN_DIFF_COEFF_VERTICAL',kvmin,1,istat,message)
  ! if(istat /= 0) call runend(-1,message)  ! Optional: no check
  !
  ! Set roughness length
  !
  if(strcompare(rough_model,'UNIFORM')) then
     ! Set uniform roughness length
     rlength = rlength0
  elseif(strcompare(rough_model,'MATRIX')) then
     ! Read from file (the filename is set in BLOCK FILES)
     call read_interp_grd(frou,x0,y0,dx,dy,nx,ny,rlength)
  else
     call runend(-1,'Invalid input ROUGHNESS_MODEL')
  endif
  !
  return
end subroutine read_block_meteo
