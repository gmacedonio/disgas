subroutine vsettl(diam,rhop,rhoa,visc,vset,model,psi,ierr)
  !
  !
  !    Version: 1.1
  !
  !    Date: 15-DEC-2009
  !
  !    Set fall velocity, as a function of particles diameter and
  !    air density and viscosity.
  !
  !    NOTE: This version accounts for buoyancy force.
  !    Particle density cannot be lower than air density
  !
  !    diam     - Particle diameter in meters (Input)
  !    rhop     - Particle density (Input)
  !    rhoa     - Air density (Input)
  !    visc     - Air viscosity (Input)
  !    vset     - Particle settling velocity (Output)
  !    model    - Settling velocity model (Input)
  !    psi      - Form factor (Input)
  !    ierr     - Return status (0=OK, 1=Invalid model, 2=No convergence)
  !
  !****************************************************************************
  use kindtype
  implicit none
  !
  integer :: model
  integer :: it
  integer :: ierr
  !
  real(rp) :: diam,rhop,rhoa,visc,vset,psi
  real(rp) :: gi,atol,rtol,rey,rk1,rk2,a,b,vold,cd100,cd1000
  real(rp), save :: cd = 1.0_rp
  integer :: maxit
  !
  ierr = 0                   ! Set initial value for return status
  gi=9.81_rp                 ! Gravity acceleration
  atol = 1e-20_rp            ! Absolute tolerance
  rtol = 1.0e-7_rp           ! Relative tolerance
  maxit=10000                ! Maximum number of iterations
  !
  !
  if(model == 1) then  ! 1. Model of Arastoopour et al. 1982
     !
     vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
     vold=vset
     do it=1,maxit
        rey=rhoa*vset*diam/visc
        if(rey <= 988.947_rp) then   ! This is the actual transition point
           cd=24.0_rp/rey*(1.0_rp+0.15_rp*rey**0.687_rp)
        else
           cd=0.44_rp
        endif
        vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
        if(abs(vset-vold) <= atol+rtol*abs(vset)) return
        vold=vset
     enddo
     !
  elseif(model == 2)  then  ! 2. Model of Ganser 1993
     !
     vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
     vold=vset
     do it=1,maxit
        rey=rhoa*vset*diam/visc
        rk1=3.0_rp/(1.0_rp+2.0_rp/sqrt(psi))
        rk2=10.0_rp**(1.8148_rp*(-log10(psi))**0.5743_rp)
        cd=24.0_rp/(rey*rk1)*(1.0_rp+0.1118_rp*                  &
             (rey*rk1*rk2)**0.6567_rp)+0.4305_rp*rk2/      &
             (1.0_rp+3305.0_rp/(rey*rk1*rk2))

        vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
        if(abs(vset-vold) <= atol+rtol*abs(vset)) return
        vold=vset
     enddo
     !
     !
  elseif(model == 3) then    ! 3. Model of Wilson & Huang 1979
     vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
     vold=vset
     cd100=0.24_rp*psi**(-0.828_rp)+2.0_rp*sqrt(1.0_rp-psi)
     cd1000=1.0_rp-0.56_rp*psi
     a=(cd1000-cd100)/900.0_rp
     b=cd1000-1000.0_rp*a
     do it=1,maxit
        rey=rhoa*vset*diam/visc
        if(rey <= 100.0_rp) then
           cd=24.0_rp/rey*psi**(-0.828_rp)+2.0_rp*sqrt(1.0_rp-psi)
        elseif(rey > 100.0_rp .and. rey < 1000.0_rp) then
           cd=a*rey+b
        else
           cd=cd1000
        endif
        vset=sqrt(4.0_rp*gi*diam*(rhop-rhoa)/(3.0_rp*cd*rhoa))
        if(abs(vset-vold) <= atol+rtol*abs(vset)) return
        vold=vset
     enddo
     !
  else
     ! *** Model not set
     !
     ierr = 1
     return
  endif
  !
  !*** No convergence
  !
  ierr = 2
  return
  !
end subroutine vsettl
