subroutine exit(code)
  ! Exit from program
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 28-MAR-2018
  !
  ! The exit subroutine is provided as intrinsic in GNU fortran, but is not
  ! standard in F90, F95, F2003 and F2008.
  ! This subroutine can be used if not provided by the compiler
  ! Starting from F2008, The instruction STOP allows a variable as stop-code
  ! (only 8 bit are used)
  integer, intent(in) :: code
  if(code == 0) stop  ! Exit with code 0 and no message
  !
  stop 1              ! F77, F90, F95, F2003
  ! stop code         ! F2008
end subroutine exit
