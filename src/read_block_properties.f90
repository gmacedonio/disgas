subroutine read_block_properties
  !****************************************************************
  !*
  !*    Reads PROPERTIES BLOCK from the input file
  !*
  !****************************************************************
  use inpout
  use master, only: diam,rhop,psi,modv,disp_type,strcompare
  implicit none
  !
  character(len=100) :: message,word
  integer            :: istat
  character(len=20)  :: type_string
  !
  !***  PROPERTIES block
  !
  call get_input_cha(finp,'PROPERTIES','DISPERSION_TYPE',  &
       type_string,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  if(strcompare(type_string,'GAS')) then
     disp_type = 0            ! 0 => GAS
  elseif(strcompare(type_string,'PARTICLES')) then
     disp_type = 1            ! 1 => PARTICLES
  else
     call runend(-1,'Invalid input DISPERSION_TYPE')
  endif
  !
  if(disp_type == 1) then
     !
     call get_input_rea8(finp,'PROPERTIES','PARTICLE_DIAMETER',  &
          diam,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     !
     call get_input_rea8(finp,'PROPERTIES','PARTICLE_DENSITY',   &
          rhop,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     !
     call get_input_rea8(finp,'PROPERTIES','PARTICLE_SHAPE_PARAMETER', &
          psi,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     !
     call get_input_cha(finp,'PROPERTIES','PARTICLE_MODEL_VSET', &
          word,1,istat,message)
     if(istat /= 0) call runend(-1,message)
     if(strcompare(word,'ARASTOOPOUR')) then
        modv = 1
     elseif(strcompare(word,'GANSER')) then
        modv = 2
     elseif(strcompare(word,'WILSON_HUANG')) then
        modv = 3
     else
        call runend(-1,'Invalid flag PARTICLE_MODEL_VSET in the input file')
     end if



     !
  endif
  !
  return
end subroutine read_block_properties
