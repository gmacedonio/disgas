subroutine uppercase(str)
  ! Convert a string to uppercase
  implicit none
  character(len=*), intent(in out) :: str
  integer :: i, del

  del = iachar('a') - iachar('A')

  do i = 1, len_trim(str)
     if (lge(str(i:i),'a') .and. lle(str(i:i),'z')) then
        str(i:i) = achar(iachar(str(i:i)) - del)
     end if
  end do

  return
end subroutine uppercase

subroutine lowercase(str)
  ! Convert a string to lowercase
  implicit none
  character(len=*), intent(in out) :: str
  integer :: i, del

  del = iachar('a') - iachar('A')

  do i = 1, len_trim(str)
     if (lge(str(i:i),'A') .and. lle(str(i:i),'Z')) then
        str(i:i) = achar(iachar(str(i:i)) + del)
     end if
  end do

  return

end subroutine lowercase
