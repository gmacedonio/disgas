subroutine wrirst
  !**********************************************************
  !*
  !*    This routine writes the restart file
  !*
  !**********************************************************
  use inpout
  use master
  implicit none
  integer :: k
  !
  !***  writes the restart file
  !
  open(90,FILE=TRIM(frst),STATUS='unknown',form='unformatted',ERR=100)
  write(90) ibyr,ibmo,ibdy,ibhr,ibmi
  write(90) time
  write(90) nx,ny,nz
  write(90) dx,dy
  write(90) zlayer(1:nz)
  write(90) x0,y0
  write(90) fseqnum
  !
  do k=1,nz
     write(90) vx(1:nx,1:ny,k)
     write(90) vy(1:nx,1:ny,k)
     write(90) vz(1:nx,1:ny,k)
     write(90)  c(1:nx,1:ny,k)
  end do
  write(90) cload(1:nx,1:ny)
  !
  close(90)
  !
  return
  !
  !***  List of errors
  !
100 call runend(-1,'Error opening file: '//TRIM(frst))
  !
  return
end subroutine wrirst
