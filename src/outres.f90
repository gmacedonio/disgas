subroutine outres
  !**********************************************************
  !*
  !*    This routine outputs the results at the current time
  !*
  !**********************************************************
  use kindtype
  use inpout
  use master
  use rwgrd
  implicit none
  !
  character(len =250) :: fname
  integer :: iz
  real(rp), allocatable :: ctmp(:,:) ! Temporary one layer concentration
  real(8) :: xmax, ymax
  integer :: status
  !
  !***  writes the restart file
  !
  if(out_rst) call wrirst
  !
  !***  Writes the results files
  !
  ! Note: the sequence index (fseqnum) is incremented at each call
  !
  xmax = x0 + nx*dx    ! X0,Y0 are the coordinates of the bottom-left corner
  ymax = y0 + ny*dy
  if(out_u) then
     do iz=1,nz
        if(print_layer(iz)) then
           call formname(fname,outdir,'u_','.grd',iz,fseqnum)
           call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,vx(:,:,iz),status)
           if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
        end if
     enddo
  end if
  !
  if(out_v) then
     do iz=1,nz
        if(print_layer(iz)) then
           call formname(fname,outdir,'v_','.grd',iz,fseqnum)
           call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,vy(:,:,iz),status)
           if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
        end if
     enddo
  end if
  !
  if(out_w) then
     do iz=1,nz
        if(print_layer(iz)) then
           call formname(fname,outdir,'w_','.grd',iz,fseqnum)
           call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,vz(:,:,iz),status)
           if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
        end if
     enddo
  end if
  !
  if(out_g) then
     iz=0
     call formname(fname,outdir,'g_','.grd',iz,fseqnum)
     call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,cload,status)
     if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
  end if
  !
  if(out_c) then
     allocate(ctmp(nx,ny))   ! Allocate temporary memory for one layer
     do iz=1,nz
        if(print_layer(iz)) then
           call formname(fname,outdir,'c_','.grd',iz,fseqnum)
           ctmp = c(1:nx,1:ny,iz)   ! Exclude boundaries
           call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,ctmp,status)
           if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
        end if
     enddo
     deallocate(ctmp)
  end if
  !
  ! Increment sequence index
  !
  fseqnum = fseqnum+1
  !
  return
end subroutine outres

subroutine formname(fname,dir,prefix,postfix,layer,iseq)
  !**********************************************************
  !*
  !*   This subroutine generates the file name used by outres
  !*
  !**********************************************************
  implicit none
  character(len=*) :: fname,dir,prefix,postfix
  integer :: layer,iseq
  !
  write(fname,'(a,''/'',a,i3.3,''_'',i6.6,a)') &
       trim(dir), trim(prefix), layer, iseq, trim(postfix)
  return
end subroutine formname
