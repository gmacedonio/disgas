subroutine read_block_topo
  !****************************************************************
  !*
  !*    Reads/creates the topography
  !*
  !****************************************************************
  use kindtype
  use inpout
  use master, only: x0, y0, dx, dy, nx, ny, pi, strcompare, topg
  use rwgrd
  implicit none
  !
  character(len=250) :: fname
  character(len=100) :: message,word
  integer            :: status,ix,iy
  real(rp)           :: thx0,thy0,z0
  real(8)            :: xmax, ymax
  !
  !
  call get_input_cha(finp,'TOPOGRAPHY','EXTRACT_TOPOGRAPHY_FROM_FILE',  &
       word,1,status,message)
  if(status /= 0) call runend(-1,message)
  !
  if(strcompare(word,'YES')) then
     !
     !***   Read topography from a file
     !
     ! Note: the name of the topography file is set in BLOCK FILES
     !
     call read_interp_grd(ftop,x0,y0,dx,dy,nx,ny,topg)
     !
  elseif(strcompare(word,'NO')) then
     !
     !*** Creates topography
     !
     call get_input_rea8(finp,'TOPOGRAPHY','Z_ORIGIN_(M)',z0,1,status,message)
     if(status /= 0) call runend(-1,message)
     !
     call get_input_rea8(finp,'TOPOGRAPHY','X_SLOPE_(DEG)',thx0,1,status,message)
     if(status /= 0) call runend(-1,message)
     thx0 = tan(thx0*pi/180d0)                 ! Convert from deg to rad
     !
     call get_input_rea8(finp,'TOPOGRAPHY','Y_SLOPE_(DEG)',thy0,1,status,message)
     if(status /= 0) call runend(-1,message)
     thy0 = tan(thy0*pi/180d0)                 ! Convert from deg to rad
     !
     do iy=1,ny
        do ix = 1,nx
           topg(ix,iy) = z0 + thx0*(ix-1)*dx + thy0*(iy-1)*dy
        end do
     end do
     !
  else
     call runend(-1,'Invalid flag EXTRACT_TOPOGRAPHY_FROM_FILE in '// &
          'the input file')
  endif
  !
  !*** If necessary, outputs the domain
  !
  xmax = x0 + nx*dx
  ymax = y0 + ny*dy
  if(out_dom) then
     fname = TRIM(outdir)//'domain.grd'
     call wrigrd(fname,grd_type,nx,ny,x0,y0,xmax,ymax,topg,status)
     if(status /= 0) call runend(-1,'Error opening output file: '//TRIM(fname))
  end if
  !
  return
end subroutine read_block_topo
