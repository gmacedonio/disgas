! DISGAS-2.0 is a 3-D time-dependent Eulerian model for transport of gas
! in the atmospheric surface layer.
!
! Copyright (C) 2014, 2021, 2023 Antonio Costa and Giovanni Macedonio
!
! This program is free software: you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation, either version 3 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE.
!
! See the GNU General Public License for more details.
!
program disgas
  !
  !     disgas - 3-D advection-diffusion model for gas in the Atmospheric
  !     Boundary Layer (with terrain-following coordinates).
  !
  !     Authors:  Antonio Costa, Giovanni Macedonio,
  !     e-mail:   antonio.costa@ingv.it, giovanni.macedonio@ingv.it
  !
  !     Date of first version: 26-JUN-2003
  !     Date of this  version: 20-NOV-2023
  !
  use kindtype
  use master
  use inpout
  use m_kappa
  implicit none
  !
  real(rp) :: tprint
  integer  :: status
  !
  !  real(rp) :: maxdiv     ! Returned by ckdive
  !
  map = 0                   ! Apply Z-mapping: 0=NO; 1=YES
  !
  ! Setup
  !
  call setup
  !
  ! Print initial conditions
  !
  call outres
  call writim
  !
  ! Initializining time integration
  !
  ! Variable tbeg is initialized in setvar
  time = tbeg            ! Time is counted from beginning of simulation
  !
  meteo_time = tbeg - 1. ! Initialize (force reading at the beginning)
  icounter = 0           ! Iteration counter
  tprint = tbeg+tevery   ! Time of next print
  !
  ! Compute settling velocity of PARTICLES (if needed)
  if(disp_type == 1) call setvset  ! PARTICLES
  !
  ! DIAGNO: Set domain and open file
  if(strcompare(wind_model,'DIAGNO')) then
     wdiagno%nlog = nlog   ! Set the log file for m_dwm
     call wdiagno%set_horizontal_domain(x0, y0, nx, ny, dx, dy, status)
     if(status /= 0) call runend(-1,'Error in disgas from set_horizontal_domain')
     call wdiagno%set_vertical_domain(nz, zlayer, status)
     if(status /= 0) call runend(-1,'Error in disgas from set_vertical_domain')
     call wdiagno%ropen(fdia, status)
     if(status /= 0) then
        write(nlog,'(''  Error. Cannot open file: '',a)') trim(fdia)
        call runend(-1,'Cannot open DWM file')
     end if
  end if
  !
  ! Beginning of time integration loop
  do
     icounter = icounter + 1
     !
     ! Update wind field and diffusion coefficients
     !
     if(strcompare(wind_model,'SIMILARITY')) then
        if(time > meteo_time) then
           call readuni   ! @@@@@ TODO TIME INTERPOLATION @@@@@@
        end if
     elseif(strcompare(wind_model,'DIAGNO')) then
        call readdwm(status)
        if(status == 1) then
           call runend(-1,'Reached EOF in file DWM')
        endif
        if(disp_type == 1) vz = vz - vs   ! Consider settling velocity
     else
        call runend(-1, 'Invalid wind model')
     endif
     !
     ! If ustar and Monin-Obukov length was not provided, compute now
     if(need_ustarmol) call setustarmol
     !
     ! Compute diffusion coefficients
     call kappah(nx,ny,nz,dx,dy,vx,vy,th_model,rkh0,rkhor)
     call kappav(nx,ny,nz,zlayer,rmonin,ustar,tv_model,rkv0,rkver)
     if(verbose >= 1) then
        write(nlog,'(''   KHmin,KHmax: '',2(1x,g13.5e3))') &
             minval(rkhor),maxval(rkhor)
        write(nlog,'(''   KVmin,KVmax: '',2(1x,g13.5e3))') &
             minval(rkver),maxval(rkver)
     end if
     !
     !  Find critical time step for convergence
     call finddtc
     dt = dtc       ! Set dt = dt_critical
     !
     ! Check divergence of wind field
     ! call ckdive(nx,ny,nz,vx,vy,vz,dx,dy,dz,topg,ztop,map,maxdiv)
     !
     ! Adjust dt for printing
     dt = min(dt,tprint-time)  ! Reduce dt for printing
     !
     ! Source
     call source
     !
     ! Set Boundary conditions
     call setbc(c,nx,ny,nz,vx,vy,vz)
     !
     ! Advection X-Y
     !
     if(mod(icounter,2)==0) then   ! Alternate X/Y directions
        call advctx(c,nx,ny,nz,vx,dx,dt,work)
        call advcty(c,nx,ny,nz,vy,dy,dt,work)
     else
        call advcty(c,nx,ny,nz,vy,dy,dt,work)
        call advctx(c,nx,ny,nz,vx,dx,dt,work)
     endif
     !
     ! Advection Z
     call advctz(c,nx,ny,nz,vz,dz,dt,topg,ztop,work,map)
     !
     ! Diffusion
     call diffx(c,nx,ny,nz,dx,dt,rkhor)
     call diffy(c,nx,ny,nz,dy,dt,rkhor)
     call diffz(c,nx,ny,nz,dz,dt,rkver,topg,ztop,map)
     !
     ! Accumulation (for particles)
     if(disp_type == 1) then
        call accum(c,cload,nx,ny,nz,vz,dt)
     endif
     !
     ! Advance TIME
     time = time + dt
     !
     ! Check of total mass
     if(verbose >= 1) then
        write(nlog,'(/,'' Time:     '',g14.6e2)') time
     end if
     call ckmass(c,nx,ny,nz,dx,dy,dz,topg,ztop,map)
     if(verbose >= 1) then
        if(disp_type == 1) write(nlog,'('' Mass in the ground:     '',&
             &g13.5e3)') sum(cload)*dx*dy
        write(nlog,'('' Total source mass:      '',g13.5e3)') totemass
     end if
     !
     ! PRINT
     !
     if(time >= tprint) then
        call outres
        call writim
        tprint = tprint + tevery ! Next printing time
     endif
     !
     ! End of simulation (exit loop)
     if(time >= tbeg+trun) exit
     !
     ! Restore current time step
     dt = dtc                     ! Set to critical value
     dt = min(dt,tbeg+trun-time)  ! This is to avoid overshoot
     !
  enddo
  !
  ! Exit form disgas
  !
  call runend(1,'')
  !
end program disgas
