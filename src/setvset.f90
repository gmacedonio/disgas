subroutine setvset
  !***********************************************************************
  !*
  !*    Set terminal fall velocity in the computational domain
  !*
  !***********************************************************************
  use kindtype
  use master
  implicit none
  !
  integer  :: istat
  integer  :: i,j,k
  real(rp) :: Tair0,rho0
  real(rp) :: z,visc,tair
  real(rp) :: sigma,delta,theta
  real(rp) :: rhoair
  real(rp) :: vset
  !
  !***  Standard air properies at sea level
  !
  Tair0 = 288.15    ! Reference temperature at sea level
  rho0  = 1.2225    ! Density at reference temperature
  !
  do i=1,nx
     do j=1,ny
        !
        z = topg(i,j)-dz(0)    ! Initialize
        !
        do k=1,nz
           z = z+dz(k-1)
           call atmosphere(z,sigma,delta,theta)
           tair   = tair0*theta
           rhoair = rho0*sigma
           ! Sutherland's law
           call sutherland(tair,visc)
           !
           call vsettl(diam,rhop,rhoair,visc,vset,modv,psi,istat)
           if(istat==1) call runend(-1,'VSETTL: Invalid settl. velocity model')
           if(istat==2) call runend(-1,'VSETTL: Convergence not reached')
           !
           vs(i,j,k) = vset   ! This is positive downwards
           !
        enddo
     enddo
  enddo
  !
  return
end subroutine setvset
