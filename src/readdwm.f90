subroutine readdwm(status)
  !***************************************************************************
  !*
  !*    This routine gets meteorological data from a DWM file
  !*    Uses module m_dwm for optimizing reading and interpolation
  !*
  !*    OUTPUTS
  !*         status     ! Return status (0=OK, 1=EOF)
  !*
  !*    NOTE:
  !*
  !*    DWM (diagno) FILE FORMAT
  !*         time_dwm             ! Initial time (HHMM)
  !*         time_incr            ! Time increment (in hours)
  !*         xor,yor              ! Coordinares of the bottom-left corner (m)
  !*         nx_dwm,ny_dwm,nz_dwm ! (m)
  !*         dx_dwm,dy_dwm        ! (m)
  !*         cellzb(1:nz_dwm+1)   ! z of cell boundaries in diagno.out (m)
  !*         Zref                 ! Reference height (m)
  !*         Tinf                 !
  !*         Gamma                !
  !*         vx(1:nx,1:ny,1:nz)   ! (m/s)
  !*         vy(1:nx,1:ny,1:nz)   ! (m/s)
  !*         vz(1:nx,1:ny,1:nz)   ! (m/s)
  !*
  !*
  !*   Diagno produces wind for almost one day; time starts from 00:00
  !*   of the wind day. First wind block may start any hour of the day, but
  !*   time is counted from midnight. Duration of the block is written in
  !*   the variable TINC (typically one hour). Simulation time (variable
  !*   TIME is counted in sec and starts from the beginning of the simulation.
  !*   Until TIME <= METEO_TIME current wind block is used, otherwise
  !*   another wind block is read and METEO_TIME is updated.
  !*
  !***************************************************************************
  use kindtype
  use master, only: wdiagno,meteo_time_interval
  use master, only: zref,Tzref,Tz0,need_ustarmol,need_pres
  use master, only: ibhr,ibmi           ! Begin sim. time from midnight
  use master, only: time                ! Current simulation time (sec)
  use master, only: meteo_time          ! In sec from beginning of simulation
  use master, only: vx,vy,vz            ! Wind velocity components
  use m_interp
  use m_dwm
  implicit none
  !
  integer,  intent(out) :: status       ! Return status
  !
  real(rp) :: time1_sec           ! DWM block initial time (sec from midnight)
  real(rp) :: time2_sec           ! DWM block final   time (sec from midnight)
  real(rp) :: time_sim            ! Simulation time        (sec from midnight)
  real(rp) :: tbeg_from_midnight  ! Beginning of sim time  (sec from midnight)
  !
  ! *** Code begins here ***
  !
  ! Current simulation time (evaluated from midnight of the simulation day)
  tbeg_from_midnight = ibhr*3600.0_rp + ibmi*60.0_rp
  time_sim = time + tbeg_from_midnight
  !
  ! Get wind a current time step (time interpolate)
  call wdiagno%get(time_sim, status)
  if(status /= 0) then
     ! Error or reached EOF
     status = 1
     return
  end if
  ! Copy
  vx = wdiagno%u(:,:,:,1)
  vy = wdiagno%u(:,:,:,2)
  vz = wdiagno%u(:,:,:,3)
  !
  ! Copy global variables
  zref  = wdiagno%zref
  Tz0   = wdiagno%tinf
  Tzref = Tz0 + zref*wdiagno%tgamma   ! Evaluates Tzref
  !
  ! DWM time starts from midnight (HHMM=0000)
  time1_sec = wdiagno%tstart   ! This is the time in seconds from midnight
  ! time1_sec is the time of the beginning of the winds block from midnight
  time2_sec = time1_sec + wdiagno%tinc
  !
  ! Check TIME interval
  if(time_sim >= time1_sec .and. time_sim < time2_sec) then
     meteo_time = time_sim + meteo_time_interval  ! UPDATE METEO TIME
  endif
  !
  ! Need ustar and pressure
  need_ustarmol  = .true.
  need_pres      = .true.
  !
  status = 0   ! Return OK
  !
end subroutine readdwm
!
!
subroutine sec2hms(time,ih,im,is)
  !
  ! Convert time in seconds in hours,minutes,seconds
  !
  use kindtype
  implicit none
  real(rp), intent(in) :: time
  integer, intent(out) :: ih,im,is
  !
  ih = int(time/3600.0_rp)  ! Get hours
  im = int((time - ih*3600.0_rp)/60.0_rp)
  is =  int(time - ih*3600.0_rp - im*60.0_rp)
  !
  return
end subroutine sec2hms
