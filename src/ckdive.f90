subroutine ckdive(nx,ny,nz,vx,vy,vz,dx,dy,dz,topg,ztop,map,maxdiv)
  ! Check divergence
  !
  ! Version: 1.1
  !
  ! Date of this version: 03-MAR-2010
  !
  ! Changes:
  ! 1.1 - The maximum value of the divergence returned in the arguments
  !
  use kindtype
  implicit none
  !
  integer, intent(in)   :: nx,ny,nz,map
  real(rp), intent(in)  :: vx(nx,ny,nz),vy(nx,ny,nz),vz(nx,ny,nz)
  real(rp), intent(in)  :: ztop,dx,dy,topg(nx,ny),dz(0:nz)
  real(rp), intent(out) :: maxdiv
  !
  real(rp) :: dive,rjac
  integer  :: i,j,k
  !
  maxdiv=0.0_rp
  rjac=1.0_rp
  !
  do k=1,nz-1
     do j=1,ny-1
        do i=1,nx-1
           if(map==1) rjac=ztop/(ztop-topg(i,j))
           rjac=ztop/(ztop-topg(i,j))
           dive=(vx(i+1,j,k)-vx(i,j,k))/dx +(vy(i,j+1,k)-vy(i,j,k))/dy &
                +(vz(i,j,k+1)-vz(i,j,k))/dz(k)/rjac
           maxdiv=max(abs(dive),maxdiv)
        enddo
     enddo
  enddo
end subroutine ckdive
