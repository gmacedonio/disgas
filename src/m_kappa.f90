!> Horizontal and vertical diffusion coefficients
module m_kappa
  !
  ! Date: 20-NOV-2023
  !
  use kindtype
  implicit none
  real(rp) :: khmin, kvmin   ! Minimun diffusion ceofficients

contains

  !> Horizontal diffusion coefficients.
  !! model=0: K_h=const, model=1: K_h=Smagorinsky

  subroutine kappah(nx,ny,nz,dx,dy,vx,vy,model,rkh0,khor)
  !
  implicit none
  !
  ! I/O variables
  integer,  intent(in) :: nx,ny,nz
  integer,  intent(in) :: model       ! Turbulence model
  real(rp), intent(in) :: dx,dy
  real(rp), intent(in) :: rkh0
  real(rp), intent(in) :: vx(nx,ny,nz),vy(nx,ny,nz)
  real(rp), intent(out) :: khor(nx,ny,nz)
  !
  ! Local variables
  real(rp) :: prt,alfa,dvxdx,dvxdy,dvydx,dvydy,f1
  ! real(rp) :: vls
  integer :: i,j,k
  !
  !
  prt   =  0.95_rp             ! Turbulent Prandtl number
  alfa  =  0.28_rp             ! Empirical parameter
  f1 = alfa**2*dx*dy
  ! vls   = 5.0_rp               ! vortex length scale: should be changeable
  ! khmin = 0.075_rp*vls**1.33333_rp ! here set to the minimum of Kv
  ! khmin = 0.075_rp*(dx*dy)**0.6666667_rp
  !
  if(model==0) then
     ! Constant K
     khor = rkh0
     !
  elseif(model==1) then
     do k=1,nz
        do j=1,ny
           do i=1,nx
              if(i < nx) then
                 dvxdx=(vx(i+1,j,k)-vx(i,j,k))/dx
                 dvydx=(vy(i+1,j,k)-vy(i,j,k))/dx
              else
                 dvxdx=(vx(nx,j,k)-vx(nx-1,j,k))/dx
                 dvydx=(vy(nx,j,k)-vy(nx-1,j,k))/dx
              endif
              if(j < ny) then
                 dvxdy=(vx(i,j+1,k)-vx(i,j,k))/dy
                 dvydy=(vy(i,j+1,k)-vy(i,j,k))/dy
              else
                 dvxdy=(vx(i,ny,k)-vx(i,ny-1,k))/dy
                 dvydy=(vy(i,ny,k)-vy(i,ny-1,k))/dy
              endif
              khor(i,j,k) = f1*sqrt((dvxdx-dvydy)**2 + &
                   (dvydx+dvxdy)**2) ! Smagorinsky 2D
              khor(i,j,k) = prt*max(khor(i,j,k),khmin)
           enddo
        enddo
     enddo
  endif
  !
  return
end subroutine kappah

!> Set the vertical diffusion coefficient.
!! model=0: K_v=const and K_h=const; model=1: K_v="similarity theory"
subroutine kappav(nx,ny,nz,zlayer,rmonin,ustar,model,rkv0,kver)
  implicit none
  !
  ! I/O variables
  integer,  intent(in) :: nx,ny,nz
  integer,  intent(in) :: model          ! Turbulence model
  real(rp), intent(in) :: rkv0
  real(rp), intent(in) :: zlayer(nz)
  real(rp), intent(in) :: rmonin(nx,ny)  ! Monin-Obukov Length
  real(rp), intent(in) :: ustar(nx,ny)
  real(rp), intent(out) :: kver(nx,ny,nz)
  !
  ! Local variables
  real(rp) :: vkarm,gh,betah,prt,eta
  ! real(rp) :: vls
  integer :: i,j,k
  !
  vkarm =  0.4_rp              ! von Karman constant
  gh    = 11.6_rp              ! Stability constant (gamma_h)
  betah =  7.8_rp              ! Stability constant (beta_h)
  prt   =  0.95_rp             ! Turbulent Prandtl number
  ! vls   = 5.0_rp               ! vortex length scale: should be changeable
  ! kvmin = 0.075_rp*vls**1.3333333_rp
  !
  if(model==0) then
     ! Constant K
     kver = rkv0
     !
  elseif(model==1) then
     ! From Monin-Obukov theory order of Kv at z=dz
     ! kvmin = 0.075_rp*(zlayer(2)-zlayer(1))**(4.0_rp/3.0_rp)
     !
     ! From Jacobson M.Z., Fundamentals of atmospheric modelling,
     ! Cambridge University Press, 4th Edn., New York, 2005
     !
     ! kver is the vertical eddy diffusion coefficient for energy from
     ! eq.(8.54), with phi_h given by eq.(8.35)
     do k=1,nz
        do j=1,ny
           do i=1,nx
              eta = zlayer(k)/rmonin(i,j)  ! It is zero at z=0
              if(eta < 0.0_rp) then ! Unstable atmosphere
                 kver(i,j,k)=vkarm*ustar(i,j)*zlayer(k)*sqrt(1.0_rp-gh*eta)/prt
              elseif(eta > 0.0_rp) then ! Stable atmosphere
                 kver(i,j,k)=vkarm*ustar(i,j)*zlayer(k)/(prt+betah*eta)
              else          ! (eta==0) Neutral atmosphere
                 kver(i,j,k)=vkarm*ustar(i,j)*zlayer(k)/prt ! Becomes 0 at z=0
              endif
              kver(i,j,k) = max(kver(i,j,k),kvmin)
           enddo
        enddo
     enddo
  endif
  !
  return
end subroutine kappav

end module m_kappa
