subroutine sutherland(temp,visc)
  ! Evaluates the viscosity of a gas using the Sutherland's formula
  ! Temperature in Kelvin, viscosity in Pas
  !
  !
  !  visc = visc0*(a/b)*(T/T0)**(3/2)
  !
  !  a = T0+C
  !  b = T +C
  !
  !  C = 110.4 T0 = 288.15, visc0 = 1.78938e-5 for air (US Atm. Standard,1976)
  !  C = 120   T0 = 291.15, visc0 = 18.27e-6   for  air     (Crane, 1988)
  !  C = 111   T0 = 300.55, visc0 = 17.81e-6   for nitrogen (Crane, 1988)
  !  C = 240   T0 = 293.15, visc0 = 14.80e-6   for CO2      (Crane, 1988)
  !  C =  72   T0 = 293.85, visc0 = 8.76e-6    for hydrogen (Crane, 1988)
  !  C = 127   T0 = 292.25, visc0 = 20.18e-6   for oxygen   (Crane, 1988)
  !
  use kindtype
  implicit none
  !
  real(rp) :: temp,visc
  real(rp), parameter :: const = 110.4_rp
  real(rp), parameter :: t0    = 288.15_rp
  real(rp), parameter :: visc0 = 1.78938e-5_rp
  !
  visc = visc0*(t0+const)/(temp+const)*(temp/t0)**1.5_rp
  !
  return
end subroutine sutherland
