!> Module for reading diagno files and interpolate between two time steps
module m_dwm
  !
  ! Author: Giovanni Macedonio
  !
  ! Date of this version: 9-AUG-2023
  !
  ! Usage:
  !
  ! type(dwm) :: wind
  ! real(8) :: zlayer(nz)  ! Optional (for vertical interpolation)
  ! call wind%set_horizontal_domain(x0, y0, nx, ny, dx, dy)  ! Optional
  ! call wind%set_vertical_domain(nz, zlayer)                ! Optional
  ! call wind%ropen(finp, status)                 ! Open dwm file
  ! if(status /= 0) stop 'Cannot open wind file'
  ! time  = 0.0
  ! do
  !   call wind%get(time, status)     ! Get a time-interpolated wind field
  !   if(status /= 0) exit
  !   write(*,*) time, wind%u(1,1,1,:)
  !   time = time + 900d0
  ! end do
  !
  ! NOTE: the time-interpolated matrix is available after wind%get as:
  ! real(RP) :: wind%u(wind%nx, wind%ny, wind%nz, 3)   ! For the 3 wind directions
  ! Horizontal spatial interpolation is activated by set_horizontal_domain,
  ! to be called before ropen
  !
  use m_interp
  implicit none

  private :: get_free_unit, dwm_load_time_step, dwm_swap
  integer, parameter :: RDWM = kind(1.0)   ! This is the kind of DIAGNO matrices
  integer, private, parameter :: RP   = kind(1d0)   ! This is the kind of the interface

  !> Wind field between two time steps
  type :: dwm
     logical :: initialized = .false.
     logical :: horizontal_interp = .false.   ! Horizontal interpolation
     logical :: vertical_interp   = .false.   ! Vertical interpolation
     integer :: ninp = -1          ! File unit of the diagno file (-1=closed, 0=EOF)
     integer :: nlog = 6           ! Log file
     integer :: curr               ! Flag for swapping matrices (1 or 2)
     integer :: nx, ny, nz         ! Dimensions of the matrices
     real(RP) :: tstart = 0d0      ! NOTE: Start time in seconds (since midnight)
     real(RP) :: tend   = 0d0      ! NOTE: End time in seconds (since midnight)
     real(RP) :: tinc   = 0d0      ! Time increment (in seconds)
     real(RP) :: dx, dy
     real(RP) :: xorig, yorig
     real(RP) :: zref, tinf
     real(RP) :: tgamma          ! Gamma
     real(RP), allocatable :: zlayer(:)        ! Layer centers (nz)
     real(RP), allocatable :: work(:,:,:,:,:)  ! Work space(nx,ny,nz,dir,swap)
     real(RP), allocatable :: u(:,:,:,:)       ! Time-interpolated wind (nx,ny,nz,3)
   contains
     procedure :: set_horizontal_domain   => dwm_set_horizontal_domain
     procedure :: set_vertical_domain     => dwm_set_vertical_domain
     procedure :: ropen                   => dwm_ropen
     procedure :: free                    => dwm_free
     procedure :: get                     => dwm_get
     procedure, private :: load_time_step => dwm_load_time_step
  end type dwm

contains

  !> Initialize DWM for reading
  subroutine dwm_ropen(wfield, finp, status)
    implicit none
    class(dwm) :: wfield               !< The wind field
    integer, intent(out) :: status     !< Exit status (0=OK, 1=Error)
    character(len=*), intent(in) :: finp
    status = 0
    if(wfield%ninp > 0) return   ! Already open
    ! Get a free file unit
    call get_free_unit(wfield%ninp)
    if(wfield%ninp <= 0) then
       status = 1
       return
    end if
    ! Open the diagno file
    open(wfield%ninp, file=finp, status='old', form='unformatted', err=900)
    ! Read first time step
    wfield%curr = 2   ! Initialize = 2 so that load at curr=1 (changed by load)
    call wfield%load_time_step(status)
    if(status /= 0) return
    wfield%tstart = wfield%tend  ! Set tend=tstart for forcing loading of second step
    wfield%work(:,:,:,:,dwm_swap(wfield%curr)) = wfield%work(:,:,:,:,wfield%curr)
    return
900 write(wfield%nlog,'(''Cannot open file: '',a)') trim(finp)
    status = 1
  end subroutine dwm_ropen

  !> Close DWM file and free the memory
  subroutine dwm_free(wfield)
    implicit none
    class(dwm) :: wfield
    if(wfield%ninp > 0) close(wfield%ninp)
    wfield%ninp = -1  ! Reset to closed file
    if(allocated(wfield%zlayer)) deallocate(wfield%zlayer)
    if(allocated(wfield%work))   deallocate(wfield%work)
    if(allocated(wfield%u))      deallocate(wfield%u)
  end subroutine dwm_free

  !> Set horizontal spatial domain and activate spatial interpolation.
  !! Must be called before ropen
  !! Exit status: 0=OK, 1=error (called after ropen)
  subroutine dwm_set_horizontal_domain(wfield, xorig, yorig, nx, ny, dx, dy, status)
    implicit none
    class(dwm) :: wfield            !< The wind field
    real(rp) :: xorig, yorig        !< Coordinates of the lower-left corner
    real(rp) :: dx, dy              !< Cell size
    integer  :: nx,ny               !< Grid size
    integer, intent(out) :: status  !< Exit status
    status = 1
    if(wfield%initialized) then
       write(wfield%nlog) 'Error: Cannot call dwm_set_horizontal_domain after dwm_ropen'
       return
    end if
    wfield%xorig = xorig
    wfield%yorig = yorig
    wfield%dx    = dx
    wfield%dy    = dy
    wfield%nx    = nx
    wfield%ny    = ny
    wfield%horizontal_interp = .true.
    status = 0
  end subroutine dwm_set_horizontal_domain

  !> Set vertical spatial domain and activate vertical interpolation.
  !! Must be called before ropen
  !! Exit status: 0=OK, 1=error (called after ropen)
  subroutine dwm_set_vertical_domain(wfield, nz, zlayer, status)
    implicit none
    class(dwm) :: wfield            !< The wind field
    integer  :: nz                  !< Number of Z-layers
    real(8)  :: zlayer(nz)          !< Z layers (computational layer)
    integer, intent(out) :: status  !< Exit status
    status = 1
    if(wfield%initialized) then
       write(wfield%nlog) 'Error: Cannot call dwm_set_vertical_domain after dwm_ropen'
       return
    end if
    wfield%nz = nz
    if(allocated(wfield%zlayer)) deallocate(wfield%zlayer) ! Clear
    allocate(wfield%zlayer(nz))
    wfield%zlayer = zlayer
    wfield%vertical_interp = .true.
    status = 0
  end subroutine dwm_set_vertical_domain

  ! Load a time step from the diagno file (internal subroutine)
  !  This is called only when a new meteo block is available
  !! Return status: 0=OK, -1=error, 1=EOF, 2=Out_of_domain
  subroutine dwm_load_time_step(wfield, status)
    ! Note: the input file must be open unformatted
    implicit none
    class(dwm) :: wfield            !< The wind field
    integer, intent(out) :: status  !< Exit status (0=OK, 1=EOF, -1=error)
    real(RDWM) :: dwmtime           ! Time in the format HHMM
    real(RDWM) :: dwmtinc           ! Time increment in hours
    real(RDWM) :: xorig, yorig      ! Local values (read from file)
    real(RDWM) :: dx, dy            ! Local values (read from file)
    real(RDWM) :: zref, tinf
    real(RDWM) :: tgamma
    real(RDWM), allocatable :: lcellzb(:)
    real(RDWM), allocatable :: lwork(:,:)    ! Local wind field (read from file)
    real(RP), allocatable :: lzlayer(:)      ! Local z of layer centers
    real(RP), allocatable :: rwork3d(:,:,:)  ! DWM 3D wind with RP precision
    real(RP), allocatable :: xsrc(:), ysrc(:)
    real(RP), allocatable :: xdest(:), ydest(:), xweight(:), yweight(:), zweight(:)
    integer,  allocatable :: xindex(:), yindex(:), zindex(:)
    integer :: ihour, imin
    integer :: nx, ny, nz
    integer :: i, j, k, m, iexit, istat
    ! Check if file was open by dwm_init
    if(wfield%ninp < 0) then
       write(wfield%nlog,'(''Error in dwm_load_time_step: dwm_ropen not called'')')
       status = -1   ! Return error
       return
    end if
    ! Check if file was closed from previous EOF (ninp=0)
    if(wfield%ninp == 0) then
       ! Closed from a previous EOF
       status = 1    ! Return EOF
       return
    end if
    ! Read
    read(wfield%ninp, end=20) dwmtime
    read(wfield%ninp) dwmtinc
    read(wfield%ninp) xorig, yorig
    read(wfield%ninp) nx, ny, nz
    read(wfield%ninp) dx, dy

    if(.not.wfield%horizontal_interp) then
       ! No horizontal interpolation (copy domain)
       wfield%xorig = xorig
       wfield%yorig = yorig
       wfield%dx    = dx
       wfield%dy    = dy
       wfield%nx    = nx
       wfield%ny    = ny
    end if

    if(.not.wfield%vertical_interp) then
       ! No vertical interpolation (copy domain)
       wfield%nz    = nz
    end if

    ! Allocate memory if not yet initialized
    if(.not.wfield%initialized) then
       if(.not.allocated(wfield%zlayer)) then
          ! Could be allocated by set_vertical_domian
          allocate(wfield%zlayer(wfield%nz), STAT=istat)
          if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (zlayer)'
       end if
       allocate(wfield%work(wfield%nx, wfield%ny, wfield%nz, 3, 2), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (work)'
       allocate(wfield%u(wfield%nx, wfield%ny, wfield%nz, 3), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (u)'
       wfield%initialized = .true.
    end if

    ! Process time
    ihour = nint(dwmtime/100.)                  ! This the hour (0-23)
    imin  = nint(dwmtime - ihour*100)           ! Minutes
    wfield%tstart = wfield%tend                 ! Tstart is the previous tend
    wfield%tinc = dwmtinc*3600d0                ! Tinc in seconds
    wfield%tend = ihour*3600d0 + imin*60d0      ! Tend in seconds

    ! Read other parameters
    allocate(lcellzb(nz+1), STAT=istat)
    if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (lcellzb)'
    allocate(lzlayer(nz), STAT=istat)
    if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (lzlayer)'
    read(wfield%ninp) (lcellzb(i), i=1, nz+1)   ! NOTE: nz+1
    read(wfield%ninp) zref
    read(wfield%ninp) tinf
    read(wfield%ninp) tgamma
    wfield%zref   = zref     ! Copy and convert precision
    wfield%tinf   = tinf
    wfield%tgamma = tgamma
    lzlayer = 0.5_rp*(lcellzb(1:nz) + lcellzb(2:nz+1))

    ! Vertical interpolation
    if(wfield%vertical_interp) then
       ! Generate indexes and weights (X)
       allocate(zindex(wfield%nz), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (zindex)'
       allocate(zweight(wfield%nz), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (zweight)'
       call find_weights_nonuniform(lzlayer, nz, wfield%zlayer, &
            wfield%nz, zindex, zweight, iexit)
       if(iexit /= 0) then
          write(wfield%nlog,*) 'ERROR: Insufficient domain in DWM file'
          write(wfield%nlog,*) 'Error in m_dwm from find_weight_nonuniform (Z): ',iexit
          write(wfield%nlog,*) 'Z Lower bound (DWM):  ', lcellzb(1)
          write(wfield%nlog,*) 'Z Lower bound (request): ', wfield%zlayer(1)
          write(wfield%nlog,*) 'Z Upper bound (DWM):  ', lcellzb(nz+1)
          write(wfield%nlog,*) 'Z Upper bound (request): ', wfield%zlayer(wfield%nz)
          status = 2
          return
       end if
    else
       wfield%zlayer = lzlayer  ! Copy
    end if
    deallocate(lcellzb)
    deallocate(lzlayer)
    allocate(rwork3d(wfield%nx, wfield%ny, nz), STAT=istat)
    if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (rwork3d)'

    ! Horizontal interpolation
    if(wfield%horizontal_interp) then
       ! Generate source (dwm) coordinates
       allocate(xsrc(nx), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (xsrc)'
       allocate(ysrc(ny), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (ysrc)'
       do i=1, nx
          xsrc(i) = xorig + (i-0.5_rp)*dx   ! Center of the cell
       end do
       do j=1, ny
          ysrc(j) = yorig + (j-0.5_rp)*dy
       end do
       ! Generate destination (requested) coordinates
       allocate(xdest(wfield%nx), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (xdest)'
       allocate(ydest(wfield%ny), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (ydest)'
       do i = 1, wfield%nx
          xdest(i) = wfield%xorig + (i-0.5_rp)*wfield%dx  ! Center of the cells
       end do
       do j = 1, wfield%ny
          ydest(j) = wfield%yorig + (j-0.5_rp)*wfield%dy  ! Center of the cells
       end do
       ! Generate indexes and weights (X)
       allocate(xindex(wfield%nx), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (xindex)'
       allocate(xweight(wfield%nx), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (xweight)'
       call find_weights_nonuniform(xsrc, nx, xdest, wfield%nx, xindex, xweight, iexit)
       if(iexit /= 0) then
          write(wfield%nlog,*) 'ERROR: Insufficient domain in DWM file'
          write(wfield%nlog,*) 'Error in m_dwm from find_weight_nonuniform (X): ',iexit
          write(wfield%nlog,*) 'X Lower bound (DWM):  ', xsrc(1)
          write(wfield%nlog,*) 'X Lower bound (request): ', xdest(1)
          write(wfield%nlog,*) 'X Upper bound (DWM):  ', xsrc(nx)
          write(wfield%nlog,*) 'X Upper bound (request): ', xdest(wfield%nx)
          status = 2
          return
       end if
       ! Generate indexes and weights (Y)
       allocate(yindex(wfield%ny), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (yindex)'
       allocate(yweight(wfield%ny), STAT=istat)
       if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (yweight)'
       call find_weights_nonuniform(ysrc, ny, ydest, wfield%ny, yindex, yweight, iexit)
       if(iexit /= 0) then
          write(wfield%nlog,*) 'ERROR: Insufficient domain in DWM file'
          write(wfield%nlog,*) 'Error in m_dwm from find_weight_nonuniform (Y): ',iexit
          write(wfield%nlog,*) 'Y Lower bound (DWM):  ', ysrc(1)
          write(wfield%nlog,*) 'Y Lower bound (request): ', ydest(1)
          write(wfield%nlog,*) 'Y Upper bound (DWM):  ', ysrc(ny)
          write(wfield%nlog,*) 'Y Upper bound (request): ', ydest(wfield%ny)
          status = 2
          return
       end if
    end if

    ! Load wind matrices in the work space (U, V, W)
    wfield%curr = dwm_swap(wfield%curr)   ! Swap current work matrix

    ! Local memory for a wind velocity component
    allocate(lwork(nx, ny), STAT=istat)
    if(istat /= 0)  stop 'dwm_load_time_step: Cannot allocate memory (lwork)'

    ! Loop on the wind velocity components
    do m = 1,3
       ! Read
       do k=1,nz
          do j=1,ny
             read(wfield%ninp) (lwork(i,j), i=1,nx)
          end do
          !
          ! Horizontal interpolation
          if(wfield%horizontal_interp) then
             call interpolate_2d(nx, ny, real(lwork, kind=rp), wfield%nx, wfield%ny, &
                  rwork3d(:,:,k), xindex, xweight, yindex, yweight)
          else
             rwork3d(:,:,k) = lwork
          end if
       end do
       ! Vertical interpolation
       if(wfield%vertical_interp) then

          do k=1,wfield%nz
             do j=1,wfield%ny
                do i=1,wfield%nx
                   wfield%work(i,j,k,m,wfield%curr) = rwork3d(i,j,zindex(k))*zweight(k) &
                        + rwork3d(i,j,zindex(k)+1)*(1d0-zweight(k))
                end do
             end do
          end do
       else
          wfield%work(:,:,:,m,wfield%curr) = rwork3d
       end if
    end do
    ! Clean memory
    if(allocated(xsrc))    deallocate(xsrc)
    if(allocated(ysrc))    deallocate(ysrc)
    if(allocated(xdest))   deallocate(xdest)
    if(allocated(ydest))   deallocate(ydest)
    if(allocated(xindex))  deallocate(xindex)
    if(allocated(yindex))  deallocate(yindex)
    if(allocated(zindex))  deallocate(zindex)
    if(allocated(xweight)) deallocate(xweight)
    if(allocated(yweight)) deallocate(yweight)
    if(allocated(zweight)) deallocate(zweight)
    if(allocated(lwork))   deallocate(lwork)
    if(allocated(rwork3d)) deallocate(rwork3d)

    ! Success exit
    status = 0
    return

    ! Reached EOF
20  continue
    status = 0  ! Return OK (also if this time has reached EOF)
    wfield%tstart = wfield%tend
    wfield%tend = wfield%tstart + wfield%tinc
    ! Copy matrix (assume constant wind in the last step)
    wfield%work(:,:,:,:,dwm_swap(wfield%curr)) = wfield%work(:,:,:,:,wfield%curr)
    wfield%curr = dwm_swap(wfield%curr)
    if(wfield%ninp > 0) close(wfield%ninp)
    wfield%ninp = 0  ! Set EOF
  end subroutine dwm_load_time_step

  !> Get a wind matrix at a given time.
  ! This subroutine performs interpolation in time
  subroutine dwm_get(wind, time, status)
    implicit none
    class(dwm) :: wind
    real(RP), intent(in)  :: time
    integer, intent(out) :: status
    real(RP) :: wgt
    status = 0
    do while(time > wind%tend)
       call wind%load_time_step(status)
       if(status < 0) then
          write(wind%nlog, '(''Error in dwm_get: file closed'')')
          return
       end if
       if(status == 1) exit   ! EOF
       if(status == 2) return ! Out of domain
    end do
    wgt = 0.0_rp
    if(wind%tend > wind%tstart) wgt = (time-wind%tstart)/(wind%tend-wind%tstart)
    wind%u = (1.0_rp-wgt)*wind%work(:,:,:,:,dwm_swap(wind%curr)) + &
         wgt*wind%work(:,:,:,:,wind%curr)
  end subroutine dwm_get

  subroutine get_free_unit(unit)
    ! Returns the first free file unit number (returns -1 if not found)
    implicit none
    integer, intent(out) :: unit
    logical :: lod
    integer :: status
    integer :: i
    unit = -1
    do i = 2, 1000                                           ! Start from unit=2
       if(i == 5 .or. i == 6 .or. i == 7 .or. i == 9) cycle  ! Skip 5,6,7,9
       inquire(i, opened=lod, iostat = status)
       if(status /= 0) cycle
       if(.not.lod) then
          unit=i
          return
       end if
    end do
  end subroutine get_free_unit

  integer function dwm_swap(curr)
    ! Swap
    implicit none
    integer, intent(in) :: curr
    dwm_swap = mod(curr,2) + 1  ! Change swap flag
  end function dwm_swap

end module m_dwm
