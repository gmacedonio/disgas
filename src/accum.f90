subroutine accum(c,cload,nx,ny,nz,vz,dt)
  !*************************************************************************
  !*
  !*    Computes ground accumulation
  !*
  !*************************************************************************
  use kindtype
  implicit none
  !
  integer  :: nx,ny,nz
  integer  :: i,j                        ! Work integers
  real(rp) :: vz(nx,ny,nz)               ! Vertical velocity
  !
  real(rp) :: dt
  real(rp) :: c(0:nx+1,0:ny+1,0:nz+1)
  real(rp) :: cload(nx,ny)
  real(rp) :: vzeta
  !
  do i=1,nx
     do j=1,ny
        vzeta = min(0.0_rp,vz(i,j,1))  ! Consider only vz < 0
        cload(i,j) = cload(i,j) - dt*0.5_rp*(c(i,j,1)+c(i,j,0))*vzeta
     enddo
  enddo
  !
  return
end subroutine accum
