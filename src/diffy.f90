subroutine diffy(c,nx,ny,nz,dy,dt,rkhor)
  ! Diffusion (Y)
  use kindtype
  implicit none
  !
  integer, intent(in) :: nx,ny,nz
  real(rp), intent(in) :: dy,dt
  real(rp), intent(in) :: rkhor(nx,ny,nz)
  real(rp), intent(inout) :: c(0:nx+1,0:ny+1,0:nz+1)
  !
  real(rp), parameter :: D8MIN=1e-13_rp
  real(rp) :: dtdy2,rky1,rky2,rks1,rks2
  integer :: i,j,k,jm1,jp1
  logical :: arithmetic_mean = .true.
  !
  dtdy2=dt/dy**2
  !
  do k=1,nz
     do i=1,nx
        do j=1,ny
           jm1=max(1,j-1)
           jp1=min(j+1,ny)
           !
           rks1=0.5_rp*(rkhor(i,jm1,k)+rkhor(i,j,k))
           rks2=0.5_rp*(rkhor(i,j,k)+rkhor(i,jp1,k))
           !
           if(arithmetic_mean) then
              ! Aritmetic mean
              rky1=rks1
              rky2=rks2
           else
              ! Geometric mean
              if(rks1 > D8MIN) then
                 rky1=(rkhor(i,jm1,k)*rkhor(i,j,k))/rks1
              else
                 rky1=0.0_rp
              endif
              if(rks2 > D8MIN) then
                 rky2=(rkhor(i,j,k)*rkhor(i,jp1,k))/rks2
              else
                 rky2=0.0_rp
              endif
           endif
           !
           c(i,j,k) = c(i,j,k) + dtdy2*(rky1*c(i,j-1,k) - &
                (rky1+rky2)*c(i,j,k)+rky2*c(i,j+1,k))
        enddo
     enddo
  enddo
  return
end subroutine diffy
