subroutine setbc(c,nx,ny,nz,vx,vy,vz)
  ! Set the Boundary conditions
  !
  use KindType
  implicit none
  !
  integer :: nx,ny,nz
  real(rp) :: c(0:nx+1,0:ny+1,0:nz+1)
  real(rp) :: vx(nx,ny,nz),vy(nx,ny,nz),vz(nx,ny,nz)
  integer :: i,j,k
  !
  ! Bottom/Top (Z)
  !
  do j=1,ny
     do i=1,nx
        if(vz(i,j,1) > 0.0_rp) then
           c(i,j,0) = 0.0_rp
        else
           c(i,j,0)    = c(i,j,1) ! Null gradient
        endif
        if(vz(i,j,nz) >= 0.0_rp) then
           c(i,j,nz+1) = c(i,j,nz) ! Free flow
        else
           c(i,j,nz+1) = 0.0_rp
        endif
     enddo
  enddo
  !
  ! Left/Right (X)
  !
  do k=1,nz
     do j=1,ny
        if(vx(1,j,k) > 0.0_rp) then
           c(0,j,k)    = 0.0_rp
        else
           c(0,j,k)    = c(1,j,k)
        endif
        if(vx(nx,j,k) >= 0.0_rp) then
           c(nx+1,j,k) = c(nx,j,k)
        else
           c(nx+1,j,k) = 0.0_rp
        endif
     enddo
  enddo
  !
  ! Up/Down (Y)
  !
  do k=1,nz
     do i=1,nx
        if(vy(i,1,k) > 0.0_rp) then
           c(i,0,k)    = 0.0_rp
        else
           c(i,0,k)    = c(i,1,k)
        endif
        if(vy(i,ny,k) >= 0.0_rp) then
           c(i,ny+1,k) = c(i,ny,k)
        else
           c(i,ny+1,k) = 0.0_rp
        endif
     enddo
  enddo
  !
  ! Corners
  !
  c(0,0,0)          = c(1,1,1)
  c(nx+1,0,0)       = c(nx,1,1)
  c(0,ny+1,0)       = c(1,ny,1)
  c(nx+1,ny+1,0)    = c(nx,ny,1)
  c(0,0,nz+1)       = c(1,1,nz)
  c(nx+1,0,nz+1)    = c(nx,1,nz)
  c(0,ny+1,nz+1)    = c(1,ny,nz)
  c(nx+1,ny+1,nz+1) = c(nx,ny,nz)
  !
  return
end subroutine setbc
