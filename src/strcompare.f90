function strcompare(str1,str2)
  ! Compare two strings
  !
  ! WARNING: Strings are first converted to uppercase
  !
  logical :: strcompare
  character(len=*) :: str1,str2
  !
  call uppercase(str1)
  call uppercase(str2)
  !
  if(TRIM(str1) == TRIM(str2)) then
     strcompare = .true.
  else
     strcompare = .false.
  endif
end function strcompare
