subroutine read_block_grid
  !****************************************************************
  !*
  !*    Reads GRID BLOCK from the input file and allocates memory
  !*
  !****************************************************************
  use inpout
  use master, only: nx,ny,nz,dx,dy,x0,y0,zlayer,dz
  implicit none
  !
  character(len=100) :: message
  integer            :: k,istat,npar
  !
  !*  GRID BLOCK (except topography-related variables)
  !
  call get_input_int4(finp,'GRID','NX',nx,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'GRID','NY',ny,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'GRID','NZ',nz,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'GRID','DX_(M)',dx,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'GRID','DY_(M)',dy,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_npar(finp,'GRID','Z_LAYERS_(M)',npar,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(npar /= nz) then
     message = 'Number of Z_LAYERS must be equal to NZ'
     call runend(-1,message)
  else
     ! Allocate memory for zlayer
     allocate(zlayer(nz),STAT=istat)
     if(istat /= 0) call runend(-1,'Cannot allocate memory for vector: zlayer')
     ! Allocate memory for dz
     allocate(dz(0:nz),STAT=istat)
     if(istat /= 0) call runend(-1,'Cannot allocate memory for vector: dz')
     !
     call get_input_vec8(finp,'GRID','Z_LAYERS_(M)',zlayer,nz,istat,message)
     if(istat /= 0) call runend(-1,message)
     !
     ! Computes dz
     !
     do k=1,nz-1
        dz(k)=(zlayer(k+1)-zlayer(k))
     enddo
     dz(0)=dz(1)               ! Extend to outer layers
     dz(nz)=dz(nz-1)
  endif
  !
  ! Origin UTM of coordinates (bottom-left corner)
  call get_input_rea8(finp,'GRID','X_ORIGIN_(UTM_M)',x0,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'GRID','Y_ORIGIN_(UTM_M)',y0,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  return
end subroutine read_block_grid
