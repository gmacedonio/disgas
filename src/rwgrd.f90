module rwgrd
  ! Low-lever read and write Golden Surfer Grids
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 10-OCT-2021
  !
  ! The coordinates xmin,ymin refer to the bottom-left corner

contains

  subroutine readgrd(ninp,nx,ny,xmin,ymin,xmax,ymax,zmin,zmax,matrix,status)
    ! Read a GRD file and store in a real matrix
    ! The memory for the matrix is allocated by this routine
    ! NOTE: xmin,xmax,ymin,ymax are real(8)
    ! Status: 0=ok, 1,2=error
    !
    use kindtype
    implicit none
    integer, intent(in)  :: ninp    ! Input file unit
    integer, intent(out) :: nx,ny
    integer, intent(out) :: status
    real(rp), pointer :: matrix(:,:)
    real(rp), intent(out) :: zmin,zmax
    real(kind=8), intent(out) :: xmin,xmax,ymin,ymax ! Note: real*8
    real(rp) :: dx, dy
    !
    ! Local variables
    character(len=4) :: magic   ! The magic number
    integer :: i,j,istat
    !
    read(ninp,'(a4)',advance='no') magic
    !
    if(magic=='DSAA') then
       !
       ! ASCII VERSION
       !
       read(ninp,*) nx,ny
       read(ninp,*) xmin,xmax,ymin,ymax,zmin,zmax  ! Here center of the LB pixel
       ! Note: In Golden Surfer grids the coordinates xmin,ymin refer to
       ! the center of the bottom-left pixel: add offsets
       dx = (xmax-xmin)/(nx-1)
       dy = (ymax-ymin)/(ny-1)
       xmin = xmin - 0.5_rp*dx
       xmax = xmax + 0.5_rp*dx
       ymin = ymin - 0.5_rp*dy
       ymax = ymax + 0.5_rp*dy
       !
       ! Now allocate memory
       allocate(matrix(nx,ny),STAT=istat)

       if(istat /= 0) then
          status = 1    ! Cannot allocate memory
          return
       endif
       read(ninp,*) ((matrix(i,j),i=1,nx),j=1,ny) ! Ascii file is easy to read
       !
    else
       !
       ! Not ascii GRD file or not supported
       !
       status = 2
       return
    endif

    status = 0  ! Normal termination
    return
  end subroutine readgrd

  subroutine wrigrd(fname,grd_type,nx,ny,xmin,ymin,xmax,ymax,f,status)
    ! Writes GRD file in format suitable for Surfer(tm)
    ! The coordinates xmin,ymin refer to the bottom-left corner
    ! The coordinates xmax,ymax refer to the upper-right corner
    ! NOTE: xmin,xmax,ymin,ymax are real(8)
    use kindtype
    implicit none
    integer, intent(in)  :: nx,ny
    integer, intent(in)  :: grd_type
    real(8), intent(in)  :: xmin,xmax,ymin,ymax
    real(rp), intent(in) :: f(nx,ny)
    integer, intent(out) :: status
    character(len=*), intent(in) :: fname
    real(rp) :: dx, dy
    integer :: i,j
    !
    ! GRD Types
    integer(kind=2) :: nxs,nys
    real(rp) :: zmin, zmax
    !
    ! Copy to correct type
    nxs = int(nx, kind=2)
    nys = int(ny, kind=2)
    zmin = minval(f)
    zmax = maxval(f)
    ! Note: In Golden Surfer grids the coordinates xmin,ymin refer to
    ! the center of the bottom-left pixel: add offsets
    dx = (xmax-xmin)/nx
    dy = (ymax-ymin)/ny
    !
    ! Select file type [0=ASCII, 1=BINARY]
    !
    !  ASCII TYPE
    !
    if(grd_type == 0) then
       !***  Opens the file
       open(90,FILE=TRIM(fname),err=100,STATUS='unknown')
       !***  Starts to write
       write(90,'(a4          )') 'DSAA'
       write(90,'(i4  ,1x,i4  )') nxs,nys
       write(90,'(f9.1,1x,f9.1)') xmin+0.5d0*dx, xmax-0.5d0*dx
       write(90,'(f9.1,1x,f9.1)') ymin+0.5d0*dy, ymax-0.5d0*dy
       write(90,'(e13.5,1x,e13.5)') zmin,zmax
       !
       do j=1,ny
          write(90,10) (real(f(i,j)),i=1,nx)
       end do
       close(90)
       !
10     format(10000(e13.5,1x))
       !
       ! BINARY TYPE
       !
    elseif(grd_type == 1) then
       open(90,FILE=TRIM(fname),err=100,STATUS='unknown')
       nxs = int(nx, kind=2)
       nys = int(ny, kind=2)
       write(90,11,advance='no') 'DSBB',nxs,nys,xmin,xmax,ymin,ymax,zmin,zmax, &
            ((real(f(i,j)),i=1,nx),j=1,ny)
11     format(a4,2a2,6a8,10000(10000(a4)))
       close(90)
    endif
    !
    status = 0
    return
    !
    !***  List of errors
    !
100 status = 1
    return
  end subroutine wrigrd

end module rwgrd
