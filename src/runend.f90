subroutine runend(iflag,message)
  !************************************************************************
  !*
  !*   Ends the program
  !*
  !************************************************************************
  use inpout
  use master
  implicit none
  !
  integer   :: iflag, iexit
  character :: message*(*)
  !
  !***  Writes header
  !
  write(nlog,1)
1 format(/,                     &
       '-------------------',/, &
       ' END OF SIMULATION ',/, &
       '-------------------',/)
  !
  !***  Writes EOF
  !
  call cpu_time(cpu_time_end)
  !
  if(iflag.eq.-1) then
     write(nlog,11) message,(cpu_time_end-cpu_time_start)
11   format('  STATUS  : ABNORMAL TERMINATION',/,&
          '  ERROR   : ',a                  ,/,&
          '  CPU TIME: ',f9.1, 'sec')
     iexit = 1
  else
     write(nlog,21) (cpu_time_end-cpu_time_start)
21   format('  STATUS  : NORMAL TERMINATION',/, &
          '  CPU TIME: ',f9.1,' sec')
     iexit = 0
  end if
  !
  close(nlog)

  call exit(iexit)
  !
end subroutine runend
