subroutine read_block_time
  !****************************************************************
  !*
  !*    Reads TIME BLOCK from the input file
  !*
  !****************************************************************
  use inpout
  use master, only: ibyr,ibmo,ibdy,ibhr,ibmi,trun,restart,resettime
  use master, only: strcompare
  implicit none
  !
  character(len=100) :: message,word
  integer            :: istat
  !
  !***  TIME block
  !
  call get_input_int4(finp,'TIME','YEAR',ibyr,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'TIME','MONTH',ibmo,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'TIME','DAY',ibdy,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'TIME','HOUR',ibhr,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_int4(finp,'TIME','MINUTE',ibmi,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_rea8(finp,'TIME','SIMULATION_INTERVAL_(SEC)',trun,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  !
  call get_input_cha (finp,'TIME','RESTART_RUN',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     restart = .true.
  elseif(strcompare(word,'NO')) then
     restart = .false.
  else
     call runend(-1,'Invalid flag RESTART_RUN in the input file')
  endif
  !
  call get_input_cha (finp,'TIME','RESET_TIME',word,1,istat,message)
  if(istat /= 0) call runend(-1,message)
  if(strcompare(word,'YES')) then
     resettime = .true.
  elseif(strcompare(word,'NO')) then
     resettime = .false.
  else
     call runend(-1,'Invalid flag RESET_TIME in the input file')
  endif
  !
  return
end subroutine read_block_time
