subroutine finddtc
  ! Find maximum dt for convergence
  !
  use kindtype
  use master
  implicit none
  !
  real(rp), parameter :: D8MIN=1e-13_rp
  integer :: i,j,k
  real(rp) :: dtcn,dtcni,rjac,ddz,fx1,fx2,fy1,fy2,fz1,fz2
  !
  dtcn = 0.0_rp
  rjac = 1.0_rp
  fx1 = 1.0_rp/dx
  fx2 = 2.0_rp/dx**2
  fy1 = 1.0_rp/dy
  fy2 = 2.0_rp/dy**2
  do k=1,nz
     ddz = 0.5_rp*(dz(k-1)+dz(k)) ! @@@ Controllare il DZ @@@
     fz1 = 1.0_rp/ddz
     fz2 = 2.0_rp/ddz**2
     do j=1,ny
        do i=1,nx
           if(map==1) rjac=ztop/(ztop-topg(i,j))
           dtcni = fx2*rkhor(i,j,k) + fx1*abs(vx(i,j,k)) &
                + fy2*rkhor(i,j,k) + fy1*abs(vy(i,j,k)) &
                + fz2*rjac**2*rkver(i,j,k) + fz1*abs(rjac*vz(i,j,k))
           dtcn=max(dtcn,dtcni)
        enddo
     enddo
  enddo
  !
  dtcn=max(dtcn,D8MIN)
  dtc = 1.0_rp/dtcn
  !
  return
end subroutine finddtc
